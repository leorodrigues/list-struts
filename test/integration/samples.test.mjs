import chai from 'chai';
import path from 'path';
import fs from 'fs';
import { loadJson } from '@leorodrigues/test-support';

import { parse, makeObjectAssemblingAndMergingFns, makeObjectAssemblingFns }
    from '../../index.mjs';

const { expect } = chai;

const dirname = import.meta.dirname;

const SCHEMA_PATH = path.join(dirname, './samples.sch');
const PEOPLE_LIST_PATH = path.join(dirname, './people.list');
const INVOICES_LIST_PATH = path.join(dirname, './invoices.list');

const rawSchemas = fs.readFileSync(SCHEMA_PATH).toString();
const rawPeopleList = fs.readFileSync(PEOPLE_LIST_PATH).toString();
const rawInvoicesList = fs.readFileSync(INVOICES_LIST_PATH).toString();

const expectedPeople = loadJson(path.join(dirname, './people.json'));
const expectedInvoices = loadJson(path.join(dirname, './invoices.json'));
const expectedAllInstances = loadJson(path.join(dirname, './all-instances.json'));

describe('Integration testing', () => {
    const schemas = parse(rawSchemas);
    const people = parse(rawPeopleList);
    const invoices = parse(rawInvoicesList);

    describe('Parse a schema and assemble objects', () => {
        const assemble = makeObjectAssemblingFns(schemas);

        it('Should run ok', () => {
            const assembledPeople = people.map(p => assemble.person(p));
            const assembledInvoices = invoices.map(i => assemble.invoice(i));
            expect(assembledPeople).to.be.deep.equal(expectedPeople);
            expect(assembledInvoices).to.be.deep.equal(expectedInvoices);
        });
    });

    describe('Parse a schema, assemble objects and merge the results', () => {
        const [assemble, merge] = makeObjectAssemblingAndMergingFns(schemas);

        it('Should run ok', () => {
            const allInstances = {};
            const assembledPeople = people.map(p => assemble.person(p));
            const assembledInvoices = invoices.map(i => assemble.invoice(i));
            merge.person(assembledPeople, allInstances);
            merge.invoice(assembledInvoices, allInstances);
            expect(allInstances).to.be.deep.equal(expectedAllInstances);
        });
    });
});