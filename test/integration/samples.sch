[person
    [key id choose-new]
    [fields
        [id [digest sha1 [name] [age] [address]]]
        [name [type string]]
        [age [type number ]]
        [address [refer address detached]]]]

[address
    [key id]
    [fields
        [id [digest md5 [zip] [number]]]
        [zip [type number]]
        [number [type number]]
        [street [type string]]
        [state [type string [pattern "[A-Z]{2}"]]]
        [country [type string [pattern "[A-Z]{2}"]]]]]

[people-relation
    [key id]
    [fields
        [id [type number]]
        [name [type string]]
        [left-person [refer person detached]]
        [right-person [refer person detached]]]]

[invoice
    [key transaction-id]
    [fields
        [transaction-id [type string [pattern "\d{8}-\d{3}/[A-Z]"]]]
        [customer [refer person detached]]
        [salles-agent [refer person detached]]
        [total-amount [type number]]
        [currency [type string [pattern "[A-Z]{3}"]]]
        [details [refer invoice-element array detached]]]]

[invoice-element
    [key id]
    [fields
        [id [digest md5 [transaction-id] [product]]]
        [transaction-id [inherit invoice]]
        [unit-price [type number]]
        [unit-amount [type number]]
        [product [refer product detached]]]]

[product
    [key id]
    [fields
        [id [type number]]
        [name [type string]]
        [current-price [type number]]]]
