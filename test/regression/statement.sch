[statement
    [key id choose-new]
    [fields
        [id [digest sha1 [realms] [premisses] [facts] [hypotheses]]]
        [realms [type string array]]
        [premisses [refer formula-list detached] [options optional]]
        [facts [refer formula-list detached] [options optional]]
        [hypotheses [refer formula-list detached] [options optional]]]]

[formula-list
    [key id keep-old]
    [fields
        [id [digest sha1 [realms] [formulas]]]
        [realms [inherit statement]]
        [formulas [refer formula array detached] [options whole-content]]]]

[formula
    [key id keep-old]
    [fields
        [id [digest sha1 [original]]]
        [realms [inherit statement]]
        [original [type raw] [options whole-content]]]]
