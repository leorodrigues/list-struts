import chai from 'chai';
import path from 'path';
import fs from 'fs';

import { loadJson } from '@leorodrigues/test-support';


import { parse, makeObjectAssemblingAndMergingFns } from '../../index.mjs';

const { expect } = chai;

const dirname = import.meta.dirname;

const SCHEMA_PATH = path.join(dirname, './statement.sch');
const STATEMENT_LIST_PATH = path.join(dirname, './statement.list');
const EXPECTED_STATEMENT_PATH = path.join(dirname, './statement.json');
const EXPECTED_MERGED_STATEMENT_PATH = path.join(dirname, './statement.merged.json');

const rawSchemas = fs.readFileSync(SCHEMA_PATH).toString();
const rawStatement = fs.readFileSync(STATEMENT_LIST_PATH).toString();

const expectedStatement = loadJson(EXPECTED_STATEMENT_PATH);
const expectedMergedStatement = loadJson(EXPECTED_MERGED_STATEMENT_PATH);

describe('Regression testing - Statement', () => {
    const schemas = parse(rawSchemas);
    const statement = parse(rawStatement);

    describe('Parse a schema and assemble objects', () => {
        const [assemble, merge] = makeObjectAssemblingAndMergingFns(schemas);

        it('Should run ok', () => {
            const assembledStatement = assemble.statement(statement);
            expect(assembledStatement).to.be.deep.equal(expectedStatement);

            const allInstances = { };
            merge.statement([assembledStatement], allInstances);
            expect(allInstances).to.be.deep.equal(expectedMergedStatement);
        });
    });
});