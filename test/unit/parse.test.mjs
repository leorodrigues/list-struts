import chai from 'chai';
import parse from '../../lib/parse.mjs';

const { expect } = chai;

describe('lib/parse', () => {
    it('Should return empty expression for empty string', () => {
        expect(parse('')).to.be.deep.equal([]);
    });
    it('Should return single literal', () => {
        expect(parse('[some_literal]'))
            .to.be.deep.equal([['some_literal']]);
    });
    it('Should transform each word into a literal', () => {
        expect(parse('[some literal words]'))
            .to.be.deep.equal([['some', 'literal', 'words']]);
    });
    it('Should trim excess spaces.', () => {
        expect(parse('   [  some   [   literal   ]   words  ]  '))
            .to.be.deep.equal([['some', ['literal'], 'words']]);
    });
    it('Should transform strings into single literals', () => {
        expect(parse('[some "long [braketed] string" "and words"]'))
            .to.be.deep.equal([['some', 'long [braketed] string', 'and words']]);
    });
    it('Should take escaped chars into account', () => {
        expect(parse('[foo "some long \\"message\\""]'))
            .to.be.deep.equal([['foo', 'some long \\"message\\"']]);
    });
    it('Should parse two expressions in sequence', () => {
        expect(parse('[foo :x] [bar :y]'))
            .to.be.deep.equal([['foo', ':x'], ['bar', ':y']]);
    });
});
