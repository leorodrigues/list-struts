import chai from 'chai';
import { ErrorTracker, ExecutionPointError } from '@leorodrigues/test-support';
import { isUndefinedOrEmpty, unpack } from '../../lib/support.mjs';
import { UnpackError } from '../../lib/errors.mjs';

const { expect } = chai;
const tracker = new ErrorTracker();

describe('lib/support', () => {
    describe('#isUndefinedOrEmpty(array)', () => {
        it('Shoud return true for an undefined input', () => {
            expect(isUndefinedOrEmpty()).to.be.true;
        });
        it('Shoud return false for a defined input that is not an array', () => {
            expect(isUndefinedOrEmpty('not an array')).to.be.false;
        });
        it('Shoud return false for a defined input that is an array with elements', () => {
            expect(isUndefinedOrEmpty([1, 2, 3])).to.be.false;
        });
        it('Shoud return true for a defined input that is an empty array', () => {
            expect(isUndefinedOrEmpty([])).to.be.true;
        });
    });
    describe('#unpack(result)', () => {
        it('Should throw if the argument has no "object" attribute.', () => {
            try {
                tracker.runAndTrack(() => unpack({}));

                /* istanbul ignore next */
                throw new ExecutionPointError();
            } catch(error) {
                expect(error).to.be.instanceOf(UnpackError);
            }
        });
        it('Should do nothing if result is null or undefined', () => {
            expect(unpack()).to.be.undefined;
            expect(unpack(null)).to.be.undefined;
        });
        it('Should return the "object" attribute', () => {
            expect(unpack({object: 'fake-object'})).to.be.equal('fake-object');
        });
    });
});