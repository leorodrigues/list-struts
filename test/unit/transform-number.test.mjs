import chai from 'chai';
import { ErrorTracker, ExecutionPointError } from '@leorodrigues/test-support';
import { transformNumber } from '../../lib/transform.mjs';
import { DefinitionMismatchError } from '../../lib/errors.mjs';

const tracker = new ErrorTracker();

const { expect } = chai;

describe('lib/transform#transform-number', () => {
    afterEach(() => tracker.printErrors());

    it('Should refuse a value that does not match the number patterns', () => {
        try {
            tracker.runAndTrack(() => transformNumber('123abc'));

            /* istanbul ignore next */
            throw new ExecutionPointError();
        } catch(error) {
            expect(error).to.be.instanceOf(DefinitionMismatchError);
        }
    });
    it('Should accept a value matching the integer pattern', () => {
        const result = transformNumber('1234');
        expect(result).to.be.equal(1234);
    });
    it('Should accept a value matching the float pattern', () => {
        const result = transformNumber('1234.5');
        expect(result).to.be.equal(1234.5);
    });
});
