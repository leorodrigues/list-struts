import chai from 'chai';
import sinonChai from 'sinon-chai';

import { respondToNewReference }
    from '../../../lib/assembling/respond-to-new-reference.mjs';

const { expect } = chai;

chai.use(sinonChai);

describe('lib/assembling/respond-to-new-reference', () => {
    it('Should delegate to the detached reference handling function', () => {
        const stageTwoCalls = [ ];
        const stack = [{
            schemaName: 'the-schema',
            attributeName: 'some-attr',
            stageTwoCalls
        }];
        const assemblingFunctions = { };
        const wholeContentGraph = [ ];
        const context = { stack, assemblingFunctions, wholeContentGraph };
        const eventData = { isDetached: false };

        const f = respondToNewReference(context, eventData);

        f({ schemaName: 'the-schema', isArray: false, isOptional: true, isWholeContent: false });

        const [name, fun] = stageTwoCalls[0];

        expect(name).to.be.equal('some-attr');
        expect(fun).to.be.instanceOf(Function);
    });
    it('Should delegate to the regular reference handling function', () => {
        const stageTwoCalls = [ ];
        const stack = [{
            schemaName: 'the-schema',
            attributeName: 'some-attr',
            stageTwoCalls
        }];
        const assemblingFunctions = { };
        const wholeContentGraph = [ ];
        const context = { stack, assemblingFunctions, wholeContentGraph };
        const eventData = { isDetached: true };

        const f = respondToNewReference(context, eventData);

        f({
            schemaName: 'the-schema',
            isArray: false,
            isOptional: true,
            isWholeContent: false
        });

        const [name, fun] = stageTwoCalls[0];

        expect(name).to.be.equal('some-attr');
        expect(fun).to.be.instanceOf(Function);
    });
});
