import chai from 'chai';
import { loadJson } from '@leorodrigues/test-support';

import { respondToNewType }
    from '../../../lib/assembling/respond-to-new-type.mjs';

const { expect } = chai;

const context = { stack: [ ] };

const handleEventNewTypeFound = respondToNewType(context);

const filename = import.meta.filename.replace('test.mjs', 'data.json');
const { casesByType } =  loadJson(filename);

describe('lib/assembling/respond-to-new-type', () => {

    Object.keys(casesByType).forEach(t => {
        const cases = casesByType[t];

        describe(`Given the field type is ${t}`, function () {

            beforeEach(() => context.stack.unshift({
                attributeName: 'field', stageOneCalls: [ ]
            }));

            afterEach(() => context.stack.shift());

            for (const { inputs, outputs, title } of cases) {
                it(title, () => {
                    const { sequence, eventData } = inputs;
                    const { expectedStageOneCallName, expectedParseResult } = outputs;

                    handleEventNewTypeFound({ ...eventData, typeName: t });

                    const [name, parseSequence] = context.stack[0].stageOneCalls[0];

                    expect(name).to.be.equal(expectedStageOneCallName);
                    expect(parseSequence).to.be.an.instanceOf(Function);

                    const parseResult = parseSequence(sequence);

                    expect(parseResult).to.be.deep.equal(expectedParseResult);
                });
            }
        });
    });
});
