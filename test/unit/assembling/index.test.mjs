import chai from 'chai';
import { makeObjectAssemblingFns, makeObjectAssemblingAndMergingFns }
    from '../../../lib/assembling/index.mjs';

const { expect } = chai;

describe('lib/assembling/index', () => {
    describe('#makeObjectAssemblingFn(schemas)', () => {
        it('Should register schema events and perform a walk', () => {
            const result = makeObjectAssemblingFns([ ]);
            expect(result).to.be.deep.equal({ });
        });
    });

    describe('#makeObjectAssemblingAndMergingFns(schemas)', () => {
        it('Should register schema events and perform a walk', () => {
            const result = makeObjectAssemblingAndMergingFns([ ]);
            expect(result).to.be.deep.equal([{ }, { }]);
        });
    });
});
