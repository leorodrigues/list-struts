import chai from 'chai';
import sinon from 'sinon';
import sinonChai from 'sinon-chai';
import { ErrorTracker, ExecutionPointError } from '@leorodrigues/test-support';
import { MissingSchemaError } from '../../../lib/errors.mjs';
import { jumpToSchema, getFieldContent }
    from '../../../lib/assembling/reference-operations.mjs';

const { expect } = chai;

chai.use(sinonChai);

const tracker = new ErrorTracker();
const sandbox = sinon.createSandbox();

describe('lib/assembling/reference-operations', () => {
    afterEach(() => {
        sandbox.reset();
        tracker.printErrors();
    });

    describe('#getFieldContent', () => {
        it('Should return an item of the sequence if the content is not whole', () => {
            const sequence = [['name', 'jack']];
            const result = getFieldContent(false, sequence, 'name');
            expect(result).to.be.deep.equal(['jack']);
        });
        it('Should return the entire sequence if the content is whole', () => {
            const sequence = 'the sequence';
            const result = getFieldContent(true, sequence, 'whatever');
            expect(result).to.be.deep.equal('the sequence');
        });
    });

    describe('#jumpToSchema', () => {
        it('Should invoke the schema assembling function', () => {
            const f = sinon.stub();

            f.returns({ object: '3.14' });

            const knownSchemas = { childSchema: f };

            const result = jumpToSchema('42', 'fake-context', 'childSchema', knownSchemas);

            expect(result).to.be.deep.equals('3.14');

            expect(f).to.be.calledOnceWith('42', 'fake-context');
        });
        it('Should throw an error if the name is not found', () => {
            try {
                tracker.runAndTrack(() => jumpToSchema(
                    'fake-sequence', 'fake-context', 'childSchema', {}));

                /* istanbul ignore next */
                throw new ExecutionPointError();
            } catch(error) {
                expect(error).to.be.instanceOf(MissingSchemaError);
            }
        });
    });
});
