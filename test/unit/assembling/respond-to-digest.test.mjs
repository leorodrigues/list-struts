import chai from 'chai';
import sinon from 'sinon';
import sinonChai from 'sinon-chai';
import { respondToDigest } from '../../../lib/assembling/respond-to-digest.mjs';

const { expect } = chai;

chai.use(sinonChai);

const sandbox = sinon.createSandbox();

const stageThreeCalls = [];
const assemblingFunctions = {};
const wholeContentGraph = [];
const detachmentGraph = [];
const stack = [];
const assemblingContext = {
    stack, assemblingFunctions, wholeContentGraph, detachmentGraph
};

describe('lib/assembling/respond-to-digest', () => {
    beforeEach(() => stack.unshift({
        stageThreeCalls, attributeName: 'fake-key'
    }));

    afterEach(() => sandbox.reset());

    it('Yields an assembling function that digests a field list', () => {
        const fieldList = [];
        const currentInstance = 'fake-current-instance';

        const handleEvent = respondToDigest(assemblingContext);
        handleEvent({ algorithm: 'sha1', fieldList });

        const [attributeName, assemble] = stageThreeCalls[0];

        const result = assemble(null, currentInstance);

        expect(result).to.be.deep.equal('da39a3ee5e6b4b0d3255bfef95601890afd80709');

        expect(attributeName).to.be.equal('fake-key');
    });
});
