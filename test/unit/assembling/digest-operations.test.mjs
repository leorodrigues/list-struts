import chai from 'chai';
import { ErrorTracker, ExecutionPointError } from '@leorodrigues/test-support';
import { InvalidDigestNameError } from '../../../lib/errors.mjs';
import { selectDigestImplementation }
    from '../../../lib/assembling/digest-operations.mjs';

const { expect } = chai;

const tracker = new ErrorTracker();

describe('lib/assembling/digest-operations', () => {

    describe('#selectDigestImplementation', () => {
        it('Should return a wrapped md5 for strings', () => {
            const subject = selectDigestImplementation('md5');
            expect(subject('some text'))
                .to.be.equal('552e21cd4cd9918678e3c1a0df491bc3');
        });
        it('Should return a wrapped md5 for objects', () => {
            const subject = selectDigestImplementation('md5');
            expect(subject({data: 'some data'}))
                .to.be.equal('831e50c0d4d86f969bf95945398d90d9');
        });
        it('Should return a wrapped sha1 for strings', () => {
            const subject = selectDigestImplementation('sha1');
            expect(subject('some text'))
                .to.be.equal('37aa63c77398d954473262e1a0057c1e632eda77');
        });
        it('Should return a wrapped sha1 for objects', () => {
            const subject = selectDigestImplementation('sha1');
            expect(subject({data: 'some data'}))
                .to.be.equal('71e9ca9d2b0b7c3000c30cee44bcfb795968ba43');
        });
        it('Should throw an error if the algorithm is neither "sha1" nor "md5"', () => {
            try {
                tracker.runAndTrack(() => selectDigestImplementation('x'));

                /* istanbul ignore next */
                throw new ExecutionPointError();
            } catch(error) {
                expect(error).to.be.instanceOf(InvalidDigestNameError);
            }
        });
    });
});