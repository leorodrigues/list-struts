import sinon from 'sinon';
import chai from 'chai';
import sinonChai from 'sinon-chai';
import { mergeParseResults } from '../../../lib/assembling/merging.mjs';

const { expect } = chai;

chai.use(sinonChai);

const sandbox = sinon.createSandbox();
const findClashing = sandbox.stub();
const resolveClash = sandbox.stub();

describe('lib/assembling/merging#mergeParseResults', () => {
    afterEach(() => sandbox.reset());

    it('Should add the main object to its own slot', () => {
        expect(mergeParseResults('some-schema', [{object: 'x'}]))
            .to.be.deep.equal({'some-schema': ['x']});
    });

    it('Should add detached objects to their own slots', () => {
        const theResult = {
            object: 'x',
            detached: {'child-schema': ['y']}
        };
        const mergedInstances = {
            'some-schema': ['x'],
            'child-schema': ['y']
        };
        expect(mergeParseResults('some-schema', [theResult]))
            .to.be.deep.equal(mergedInstances);
    });

    it('Should add everything to an already existing map', () => {
        const theResult = {
            object: 'j',
            detached: {'child-schema': ['k']}
        };
        const alreadyMerged = {
            'parent-schema': ['i'],
        };
        const output = {
            'parent-schema': ['i', 'j'],
            'child-schema': ['k']
        };
        expect(mergeParseResults('parent-schema', [theResult], alreadyMerged))
            .to.be.deep.equal(output);
    });

    it('Should replace a root object if a clash is found', () => {
        findClashing.returns(0);
        resolveClash.returns('elephant');
        const result = mergeParseResults(
            'some-schema',
            [{object: 'x'}],
            {},
            resolveClash,
            findClashing);
        expect(result).to.be.deep.equal({'some-schema': ['elephant']});
    });

    it('Should replace a detached object if a clash is found', () => {
        findClashing.onCall(0).returns(-1);
        findClashing.onCall(1).returns(0);
        resolveClash.returns('elephant');
        const result = mergeParseResults(
            'some-schema',
            [{object: 'x', detached: {'child': ['y']}}],
            {},
            resolveClash,
            findClashing);
        expect(result).to.be.deep.equal({
            'some-schema': ['x'],
            'child': ['elephant']
        });
    });
});
