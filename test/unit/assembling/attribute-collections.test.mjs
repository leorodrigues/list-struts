import chai from 'chai';
import { ErrorTracker, ExecutionPointError } from '@leorodrigues/test-support';
import { AttributeNotFoundError } from '../../../lib/errors.mjs';
import { makeCollectFieldValuesFunction }
    from '../../../lib/assembling/attribute-collections.mjs';

const { expect } = chai;

const tracker = new ErrorTracker();

describe('lib/assembling/attribute-collections', () => {
    afterEach(() => tracker.printErrors());

    describe('Given the root object is defined', function () {
        it('Should collect direct and indirect values', () => {
            const fieldList = [['myId'], ['child', 'id']];
            const subjectFunction = makeCollectFieldValuesFunction(fieldList);
            const result = subjectFunction({myId: 11, child: {id: 13}});
            expect(result).to.be.deep.equal([11, 13]);
        });

        it('Should throw an error if an attribute is not found', () => {
            const fieldList = [['someId']];
            const subjectFunction = makeCollectFieldValuesFunction(fieldList);
            try {
                tracker.runAndTrack(() => subjectFunction({myId: 17}));
                /* istanbul ignore next */
                throw new ExecutionPointError();
            } catch(error) {
                expect(error).to.be.instanceOf(AttributeNotFoundError);
            }
        });

        it('Should throw an error if an indirect attribute is not found', () => {
            const fieldList = [['child', 'data']];
            const subjectFunction = makeCollectFieldValuesFunction(fieldList);
            try {
                tracker.runAndTrack(() => subjectFunction({child: {id: 19}}));
                /* istanbul ignore next */
                throw new ExecutionPointError();
            } catch(error) {
                expect(error).to.be.instanceOf(AttributeNotFoundError);
            }
        });
    });

    describe('Given the root object is undefined', function () {
        it('Should return undefined for a direct query', () => {
            const fieldList = [['myId']];
            const subjectFunction = makeCollectFieldValuesFunction(fieldList);
            const result = subjectFunction();
            expect(result).to.be.deep.equal([ undefined ]);
        });

        it('Should return undefined for an indirect query', () => {
            const fieldList = [['my-child', 'id']];
            const subjectFunction = makeCollectFieldValuesFunction(fieldList);
            const result = subjectFunction();
            expect(result).to.be.deep.equal([ undefined ]);
        });
    });
});