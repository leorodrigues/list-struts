import chai from 'chai';
import { ErrorTracker, ExecutionPointError } from '@leorodrigues/test-support';
import { UnknownClashResolutionAlgorithmError } from '../../../lib/errors.mjs';
import { selectClashResolution, makeFindClashFn }
    from '../../../lib/assembling/clash-resolution.mjs';

const { expect } = chai;

const tracker = new ErrorTracker();

describe('lib/assembling/clash-resolution', () => {
    afterEach(() => tracker.printErrors());

    describe('#makeFindClashFn(attributeName)', () => {
        it('Should return find the object by attribute', () => {
            const find = makeFindClashFn('id');
            expect(find([{id: 1}, {id: 2}], {id: 2})).to.be.equal(1);
        });
    });

    describe('#selectClashResolution(id)', () => {
        it('Should throw error for unknown algorithm names', () => {
            try {
                tracker.runAndTrack(() => selectClashResolution('fake-name'));

                /* istanbul ignore next */
                throw new ExecutionPointError();
            } catch(error) {
                expect(error).to.be.instanceOf(UnknownClashResolutionAlgorithmError);
            }
        });
        it('Should return the "choose-new" algorithm', () => {
            const resolve = selectClashResolution('choose-new');
            const newData = { data: 'new' };
            const oldData = { data: 'old' };
            expect(resolve(oldData, newData))
                .to.be.equal(newData);
        });
        it('Should return the "keep-old" algorithm', () => {
            const resolve = selectClashResolution('keep-old');
            const newData = { data: 'new' };
            const oldData = { data: 'old' };
            expect(resolve(oldData, newData))
                .to.be.equal(oldData);
        });
    });
});