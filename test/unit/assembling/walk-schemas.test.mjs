import chai from 'chai';
import sinon from 'sinon';
import sinonChai from 'sinon-chai';
import _ from 'lodash';

import { walkSchemas } from '../../../lib/assembling/control.mjs';
import { IncompleteDefinitionError } from '../../../lib/errors.mjs';
import { ErrorTracker, ExecutionPointError } from '@leorodrigues/test-support';

const { expect } = chai;

chai.use(sinonChai);

const sandbox = sinon.createSandbox();

const respondToNewSchema = sandbox.stub();
const respondToNewAttribute = sandbox.stub();
const respondToDigest = sandbox.stub();
const respondToNewReference = sandbox.stub();
const respondToInheritance = sandbox.stub();
const respondToNewType = sandbox.stub();
const respondToEndOfSchema = sandbox.stub();
const respondToKey = sandbox.stub();

function bindSchemas(events) {
    events.on('foundSchema', respondToNewSchema);
    events.on('foundAttribute', respondToNewAttribute);
    events.on('foundDigest', respondToDigest);
    events.on('foundReference', respondToNewReference);
    events.on('foundInheritance', respondToInheritance);
    events.on('foundType', respondToNewType);
    events.on('foundKey', respondToKey);
    events.on('endOfSchema', respondToEndOfSchema);
}

const tracker = new ErrorTracker();

describe('lib/assembling/control#walk-schemas', () => {
    afterEach(() => {
        sandbox.reset();
        tracker.printErrors();
    });

    it('Should throw an error when the "fields" definition is not given', () => {
        try {
            const theSchema = ['fake-schema'];
            tracker.runAndTrack(() => walkSchemas([theSchema], () => {}));

            /* istanbul ignore next */
            throw new ExecutionPointError();
        } catch(error) {
            expect(error).to.be.instanceOf(IncompleteDefinitionError);
        }
    });

    it('Should walk through a key definition', () => {
        const schema =
            ['fake-schema',
                ['key', 'some-field', 'some-strategy'],
                ['fields', ['fake-field', ['type', 'string']]]];

        const eventData = {
            isArray: false,
            isOptional: false,
            isWholeContent: false,
            parameters: [],
            typeName: 'string'
        };

        walkSchemas([schema], bindSchemas);

        expect(respondToNewSchema).to.be.calledOnceWith(sinon.match(o =>
            _.isEqual(o, {schemaName: 'fake-schema'})));

        expect(respondToNewAttribute).to.be.calledOnceWith(sinon.match(o =>
            _.isEqual(o, {attributeName: 'fake-field'})));

        expect(respondToNewType).to.be.calledOnceWith(sinon.match(o =>
            _.isEqual(o, eventData)));

        expect(respondToKey).to.be.calledOnceWith(sinon.match(o =>
            _.isEqual(o, {fieldName: 'some-field', clashResolutionId: 'some-strategy'})));

        expect(respondToEndOfSchema).to.be.calledOnceWith(sinon.match(o =>
            _.isEqual(o, {schemaName: 'fake-schema'})));

        expect(respondToNewReference)
            .to.not.be.called;

        expect(respondToInheritance)
            .to.not.be.called;

        expect(respondToDigest)
            .to.not.be.called;
    });

    it('Should walk through a type definition', () => {
        const schema =
            ['fake-schema', ['fields', ['fake-field', ['type', 'string']]]];

        const eventData = {
            isArray: false,
            isOptional: false,
            isWholeContent: false,
            parameters: [],
            typeName: 'string'
        };

        walkSchemas([schema], bindSchemas);

        expect(respondToNewSchema).to.be.calledOnceWith(sinon.match(o =>
            _.isEqual(o, {schemaName: 'fake-schema'})));
        expect(respondToNewAttribute).to.be.calledOnceWith(sinon.match(o =>
            _.isEqual(o, {attributeName: 'fake-field'})));
        expect(respondToNewType).to.be.calledOnceWith(sinon.match(o =>
            _.isEqual(o, eventData)));
        expect(respondToEndOfSchema).to.be.calledOnceWith(sinon.match(o =>
            _.isEqual(o, {schemaName: 'fake-schema'})));
        expect(respondToNewReference)
            .to.not.be.called;
        expect(respondToInheritance)
            .to.not.be.called;
        expect(respondToDigest)
            .to.not.be.called;
    });

    it('Should walk through an array type definition with options', () => {
        const schema =
            ['fake-schema',
                ['fields',
                    ['fake-field',
                        ['type', 'string', 'array', ['pattern', '\\d+']],
                        ['options', 'optional', 'whole-content']]]];

        const eventData = {
            isArray: true,
            isOptional: true,
            isWholeContent: true,
            parameters: ['\\d+'],
            typeName: 'string'
        };

        walkSchemas([schema], bindSchemas);

        expect(respondToNewSchema).to.be.calledOnceWith(sinon.match(o =>
            _.isEqual(o, {schemaName: 'fake-schema'})));
        expect(respondToNewAttribute).to.be.calledOnceWith(sinon.match(o =>
            _.isEqual(o, {attributeName: 'fake-field'})));
        expect(respondToNewType).to.be.calledOnceWith(sinon.match(o =>
            _.isEqual(o, eventData)));
        expect(respondToEndOfSchema).to.be.calledOnceWith(sinon.match(o =>
            _.isEqual(o, {schemaName: 'fake-schema'})));
        expect(respondToNewReference)
            .to.not.be.called;
        expect(respondToInheritance)
            .to.not.be.called;
        expect(respondToDigest)
            .to.not.be.called;
    });

    it('Should walk through a reference definition', () => {
        const schema =
            ['fake-schema',
                ['fields', ['fake-field', ['refer', 'child-schema']]]];

        const eventData = {
            isArray: false,
            isDetached: false,
            isOptional: false,
            isWholeContent: false,
            schemaName: 'child-schema'
        };

        walkSchemas([schema], bindSchemas);

        expect(respondToNewSchema).to.be.calledOnceWith(sinon.match(o =>
            _.isEqual(o, {schemaName: 'fake-schema'})));
        expect(respondToNewAttribute).to.be.calledOnceWith(sinon.match(o =>
            _.isEqual(o, {attributeName: 'fake-field'})));
        expect(respondToNewReference).to.be.calledOnceWith(sinon.match(o =>
            _.isEqual(o, eventData)));
        expect(respondToEndOfSchema).to.be.calledOnceWith(sinon.match(o =>
            _.isEqual(o, {schemaName: 'fake-schema'})));
        expect(respondToNewType)
            .to.not.be.called;
        expect(respondToInheritance)
            .to.not.be.called;
        expect(respondToDigest)
            .to.not.be.called;
    });

    it('Should walk through an array of reference with options', () => {
        const schema =
            ['fake-schema',
                ['fields',
                    ['fake-field',
                        ['refer', 'child-schema', 'array', 'detached'],
                        ['options', 'optional', 'whole-content']]]];

        const eventData = {
            isArray: true,
            isDetached: true,
            isOptional: true,
            isWholeContent: true,
            schemaName: 'child-schema'
        };

        walkSchemas([schema], bindSchemas);

        expect(respondToNewSchema).to.be.calledOnceWith(sinon.match(o =>
            _.isEqual(o, {schemaName: 'fake-schema'})));
        expect(respondToNewAttribute).to.be.calledOnceWith(sinon.match(o =>
            _.isEqual(o, {attributeName: 'fake-field'})));
        expect(respondToNewReference).to.be.calledOnceWith(sinon.match(o =>
            _.isEqual(o, eventData)));
        expect(respondToEndOfSchema).to.be.calledOnceWith(sinon.match(o =>
            _.isEqual(o, {schemaName: 'fake-schema'})));
        expect(respondToNewType)
            .to.not.be.called;
        expect(respondToInheritance)
            .to.not.be.called;
        expect(respondToDigest)
            .to.not.be.called;
    });

    it('Should walk through a digest definition', () => {
        const schema =
            ['fake-schema',
                ['fields',
                    ['fake-field',
                        ['digest', 'sha1', ['data'], ['fibs', 'id']]]]];

        const eventData = {
            algorithm: 'sha1',
            fieldList: [['data'], ['fibs', 'id']]
        };

        walkSchemas([schema], bindSchemas);

        expect(respondToNewSchema).to.be.calledOnceWith(sinon.match(o =>
            _.isEqual(o, {schemaName: 'fake-schema'})));
        expect(respondToNewAttribute).to.be.calledOnceWith(sinon.match(o =>
            _.isEqual(o, {attributeName: 'fake-field'})));
        expect(respondToDigest).to.be.calledOnceWith(sinon.match(o =>
            _.isEqual(o, eventData)));
        expect(respondToEndOfSchema).to.be.calledOnceWith(sinon.match(o =>
            _.isEqual(o, {schemaName: 'fake-schema'})));
        expect(respondToNewType)
            .to.not.be.called;
        expect(respondToInheritance)
            .to.not.be.called;
        expect(respondToNewReference)
            .to.not.be.called;
    });

    it('Should walk through an inheritance of same name attribute', () => {
        const schema =
            ['fake-schema',
                ['fields', ['fake-field', ['inherit', 'parent']]]];

        const eventData = {
            targetAttribute: undefined,
            targetSchema: 'parent'
        };

        walkSchemas([schema], bindSchemas);

        expect(respondToNewSchema).to.be.calledOnceWith(sinon.match(o =>
            _.isEqual(o, {schemaName: 'fake-schema'})));
        expect(respondToNewAttribute).to.be.calledOnceWith(sinon.match(o =>
            _.isEqual(o, {attributeName: 'fake-field'})));
        expect(respondToInheritance).to.be.calledOnceWith(sinon.match(o =>
            _.isEqual(o, eventData)));
        expect(respondToEndOfSchema).to.be.calledOnceWith(sinon.match(o =>
            _.isEqual(o, {schemaName: 'fake-schema'})));
        expect(respondToNewType)
            .to.not.be.called;
        expect(respondToDigest)
            .to.not.be.called;
        expect(respondToNewReference)
            .to.not.be.called;
    });

    it('Should walk through an inheritance of attribute with different name', () => {
        const schema =
            ['fake-schema',
                ['fields', ['fake-field', ['inherit', 'parent', 'data']]]];

        const eventData = {
            targetAttribute: 'data',
            targetSchema: 'parent'
        };

        walkSchemas([schema], bindSchemas);

        expect(respondToNewSchema).to.be.calledOnceWith(sinon.match(o =>
            _.isEqual(o, {schemaName: 'fake-schema'})));
        expect(respondToNewAttribute).to.be.calledOnceWith(sinon.match(o =>
            _.isEqual(o, {attributeName: 'fake-field'})));
        expect(respondToInheritance).to.be.calledOnceWith(sinon.match(o =>
            _.isEqual(o, eventData)));
        expect(respondToEndOfSchema).to.be.calledOnceWith(sinon.match(o =>
            _.isEqual(o, {schemaName: 'fake-schema'})));
        expect(respondToNewType)
            .to.not.be.called;
        expect(respondToDigest)
            .to.not.be.called;
        expect(respondToNewReference)
            .to.not.be.called;
    });
});
