import chai from 'chai';
import { ErrorTracker, ExecutionPointError } from '@leorodrigues/test-support';
import { IllegalCycleError } from '../../../lib/errors.mjs';
import {
    confirmSafeReferenceGraph,
    updateDependencyGraph
} from '../../../lib/assembling/safeguard.mjs';

const tracker = new ErrorTracker();

const { expect } = chai;

describe('lib/assembling/safeguard', () => {
    afterEach(() => tracker.printErrors());

    describe('#updateDependencyGraph', () => {
        it('Should add the given schema to the graph as a child', () => {
            const graph = [{children: []}];
            updateDependencyGraph(graph, 'child-schema', true);
            expect(graph)
                .to.be.deep.equal([{children: [{name: 'child-schema'}]}]);
        });

        it('Should not duplicate child entries', () => {
            const graph = [{children: [
                {name: 'sole-child'}
            ]}];
            updateDependencyGraph(graph, 'sole-child', true);
            expect(graph)
                .to.be.deep.equal([{children: [{name: 'sole-child'}]}]);
        });

        it('Should not update the graph because of the state', () => {
            const graph = [{children: []}];
            updateDependencyGraph(graph, 'child-schema', false);
            expect(graph).to.be.deep.equal([{children: []}]);
        });

        it('Should not fail if the grapth is undefined', () => {
            updateDependencyGraph(undefined, 'child-schema', false);
        });

        it('Should not fail if the grapth is empty', () => {
            updateDependencyGraph([], 'child-schema', false);
        });
    });

    describe('#confirmSafeReferenceGraph', () => {
        it('Should pass through if there is no cycle', () => {
            const graph = [
                {name: 'a', children: [
                    {name: 'b'}, {name: 'c'}
                ]},
                {name: 'b', children: []},
                {name: 'c', children: [
                    {name: 'd', children: []}
                ]}
            ];
            confirmSafeReferenceGraph(graph, 'test');
        });

        it('Should throw if there is a cycle', () => {
            const graph = [
                {name: 'a', children: [
                    {name: 'b'}, {name: 'c'}
                ]},
                {name: 'b', children: []},
                {name: 'c', children: [
                    {name: 'd', children: []},
                    {name: 'a', children: []}
                ]}
            ];
            try {
                tracker.runAndTrack(() => confirmSafeReferenceGraph(graph, 'test'));
                /* istanbul ignore next */
                throw new ExecutionPointError();
            } catch(error) {
                expect(error).to.be.instanceOf(IllegalCycleError);
            }
        });
    });
});
