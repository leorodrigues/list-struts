import chai from 'chai';
import { ExecutionPointError, ErrorTracker } from '@leorodrigues/test-support';
import { AncestorReferenceError } from '../../../lib/errors.mjs';
import { respondToInheritance }
    from '../../../lib/assembling/respond-to-inheritance.mjs';

const { expect } = chai;

const tracker = new ErrorTracker();
const stageOneCalls = [];
const stack = [{stageOneCalls, attributeName: 'local-data'}];
const ancestors = [
    {
        schemaName: 'parent-schema',
        object: {'local-data': 'some data', 'fib': '112358'}
    }
];

describe('lib/assembling/respond-to-inheritance', () => {
    afterEach(() => {
        tracker.printErrors();
        stageOneCalls.shift();
    });

    describe('Once the parse function is built', () => {
        it('Does nothing if the field is missing in the parent', () => {
            const eventData = {
                targetSchema: 'parent-schema',
                targetAttribute: 'missing'
            };
            const handleEvent = respondToInheritance({stack});
            handleEvent(eventData);
            const [,subjectFunc] = stageOneCalls[0];
            const result = subjectFunc(null, null, {ancestors});
            expect(result).to.be.undefined;
        });
        it('Copies data from the ancestor\'s field of the same name', () => {
            const eventData = {
                targetSchema: 'parent-schema'
            };
            const handleEvent = respondToInheritance({stack});
            handleEvent(eventData);
            const [,subjectFunc] = stageOneCalls[0];
            const result = subjectFunc(null, null, {ancestors});
            expect(result).to.be.equal('some data');
        });
        it('Copies data from an ancestor\'s field of a different name', () => {
            const eventData = {
                targetSchema: 'parent-schema',
                targetAttribute: 'fib'
            };
            const handleEvent = respondToInheritance({stack});
            handleEvent(eventData);
            const [,subjectFunc] = stageOneCalls[0];
            const result = subjectFunc(null, null, {ancestors});
            expect(result).to.be.equal('112358');
        });
        it('Throws if the ancestor is not found with the specified schema', () => {
            const eventData = {
                targetSchema: 'the-parent-schema',
                targetAttribute: 'parent-data'
            };
            const handleEvent = respondToInheritance({stack});
            handleEvent(eventData);
            const [,subjectFunc] = stageOneCalls[0];

            try {
                tracker.runAndTrack(() =>
                    subjectFunc(null, null, {ancestors}));

                /* istanbul ignore next */
                throw new ExecutionPointError();
            } catch (e) {
                expect(e).to.be.instanceOf(AncestorReferenceError);
            }
        });
    });
});
