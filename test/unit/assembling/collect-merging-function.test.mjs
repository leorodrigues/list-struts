import chai from 'chai';
import sinon from 'sinon';
import sinonChai from 'sinon-chai';
import { collectMergingFunction }
    from '../../../lib/assembling/collect-merging-function.mjs';

const { expect } = chai;

chai.use(sinonChai);

const sandbox = sinon.createSandbox();
const findClashing = sandbox.stub();

describe('lib/assembling/collect-merging-function', () => {
    const mergingFunctions = {};
    const schemaKeys = { 'fake-schema': { findClashing } };
    const context = { mergingFunctions, schemaKeys };

    const handleCollectMerging = collectMergingFunction(context);
    handleCollectMerging({ schemaName: 'fake-schema' });

    beforeEach(() => findClashing.returns(-1));

    afterEach(() => sandbox.reset());

    it('Should invoke the actual merger function', () => {
        const merge = mergingFunctions['fake-schema'];

        const instances = [ { object: 'x' } ];
        const mergedInstances = { 'fake-schema': ['y'] };

        const result = merge(instances, mergedInstances);

        expect(result).to.be.deep.equal({ 'fake-schema': ['y', 'x'] });
    });

    it('Should initialize the merged instance map', () => {
        const merge = mergingFunctions['fake-schema'];
        const instances = [ { object: 'x' } ];

        const result = merge(instances);

        expect(result).to.be.deep.equal({ 'fake-schema': ['x'] });
    });
});