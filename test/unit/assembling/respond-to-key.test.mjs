import chai from 'chai';
import { respondToKey } from '../../../lib/assembling/respond-to-key.mjs';

const { expect } = chai;

describe('lib/assembling/respond-to-key', () => {
    it('Should configure the context given a specific clash resolution id', () => {
        const context = {
            stack: [{ schemaName: 'parent-schema' }],
            schemaKeys: {}
        };

        const handleKey = respondToKey(context);
        handleKey({ fieldName: 'child-field', clashResolutionId: 'choose-new' });

        const parentSchema = context.schemaKeys['parent-schema'];

        expect(parentSchema.fieldName).to.be.equal('child-field');
        expect(parentSchema.clashResolutionId).to.be.equal('choose-new');
        expect(parentSchema.resolveClash).to.be.an.instanceOf(Function);
        expect(parentSchema.findClashing).to.be.an.instanceOf(Function);
    });

    it('Should configure the context with the default clash resolution id', () => {
        const context = {
            stack: [{ schemaName: 'parent-schema' }],
            schemaKeys: {}
        };

        const handleKey = respondToKey(context);
        handleKey({ fieldName: 'child-field' });

        const parentSchema = context.schemaKeys['parent-schema'];

        expect(parentSchema.fieldName).to.be.equal('child-field');
        expect(parentSchema.clashResolutionId).to.be.equal('keep-old');
        expect(parentSchema.resolveClash).to.be.an.instanceOf(Function);
        expect(parentSchema.findClashing).to.be.an.instanceOf(Function);
    });
});