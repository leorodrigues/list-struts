import chai from 'chai';
import { respondToNewAttribute }
    from '../../../lib/assembling/respond-to-new-attribute.mjs';

const { expect } = chai;

describe('lib/assembling/respond-to-new-attribute', () => {
    it('Yields a function that registers the current attribute name', () => {
        const stack = [{}];
        const context = {stack};
        const handleNewAttribute = respondToNewAttribute(context);
        handleNewAttribute({attributeName: 'fake-attribute'});
        expect(stack).to.be.deep.equal([{attributeName: 'fake-attribute'}]);
    });
});