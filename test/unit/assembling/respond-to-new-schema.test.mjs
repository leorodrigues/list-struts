import chai from 'chai';
import { respondToNewSchema }
    from '../../../lib/assembling/respond-to-new-schema.mjs';

const {expect} = chai;

describe('lib/assembling/respond-to-new-schema', () => {
    const stack = [];
    const wholeContentGraph = [];
    const detachmentGraph = [];
    const handleNewEschema = respondToNewSchema({
        stack, wholeContentGraph, detachmentGraph
    });
    handleNewEschema({schemaName: 'fake-schema'});

    it('Should stack a new schema layer in the context', () => {
        expect(stack).to.be.deep.equal([{
            stageOneCalls: [],
            stageTwoCalls: [],
            stageThreeCalls: [],
            schemaName: 'fake-schema'
        }]);
    });
    it('Should stack a new item in the "wholeContentGrapth"', () => {
        expect(wholeContentGraph).to.be.deep.equal([{
            name: 'fake-schema', children: []
        }]);
    });
    it('Should stack a new item in the "detachmentGraph"', () => {
        expect(detachmentGraph).to.be.deep.equal([{
            name: 'fake-schema', children: []
        }]);
    });
});