import chai from 'chai';
import sinon from 'sinon';
import sinonChai from 'sinon-chai';

import { respondToEndOfSchema }
    from '../../../lib/assembling/respond-to-end-of-schema.mjs';

const { expect } = chai;

chai.use(sinonChai);

const sandbox = sinon.createSandbox();

const stageOneField = sandbox.stub();
const stageTwoField = sandbox.stub();
const stageThreeField = sandbox.stub();
const stageOneCalls = [['f1', stageOneField]];
const stageTwoCalls = [['f2', stageTwoField]];
const stageThreeCalls = [['f3', stageThreeField]];
const assemblingFunctions = {};
const wholeContentGraph = [];
const detachmentGraph = [];
const stack = [];
const assemblingContext = {
    stack, assemblingFunctions, wholeContentGraph, detachmentGraph
};

describe('lib/assembling/respond-to-end-of-schema', () => {
    beforeEach(() =>
        stack.unshift({ stageOneCalls, stageTwoCalls, stageThreeCalls }));

    afterEach(() =>
        sandbox.reset());

    describe('Once the parse function is built and executed', () => {
        it('Should process empty sequences', () => {
            const handleEvent = respondToEndOfSchema(assemblingContext);

            handleEvent({ schemaName: 'the-schema' });

            const assemble = assemblingFunctions['the-schema'];
            const result = assemble();

            expect(stack).to.be.empty;
            expect(result).to.be.deep.equal({object: {}});
        });

        it('Should return a packed result without detached instances', () => {
            stageOneField.returns('x');
            stageTwoField.returns('y');
            stageThreeField.returns('z');

            const handleEvent = respondToEndOfSchema(assemblingContext);
            handleEvent({schemaName: 'the-schema'});
            const assemble = assemblingFunctions['the-schema'];
            const result = assemble('the sequence');

            expect(stageOneField).to.be.calledOnceWith('the sequence');
            expect(stageTwoField).to.be.calledOnceWith('the sequence');
            expect(stageThreeField).to.be.calledOnceWith('the sequence');

            expect(stack).to.be.empty;
            expect(result).to.be.deep.equal({object: {f1: 'x', f2: 'y', f3: 'z'}});
        });

        it('Should return a packed result with detached instances', () => {
            stageOneField.returns('x');
            stageTwoField.returns('y');
            stageThreeField.returns('z');
            const instances = {'another-schema':[{fibs: 112358}]};

            const handleEvent = respondToEndOfSchema(assemblingContext);
            handleEvent({schemaName: 'the-schema'});
            const assemble = assemblingFunctions['the-schema'];
            const result = assemble('the sequence', {instances});

            expect(stageOneField).to.be.calledOnceWith('the sequence');
            expect(stageTwoField).to.be.calledOnceWith('the sequence');
            expect(stageThreeField).to.be.calledOnceWith('the sequence');

            expect(stack).to.be.empty;
            expect(result)
                .to.be.deep.equal({
                    object: {f1: 'x', f2: 'y', f3: 'z'},
                    detached: {
                        'another-schema': [{fibs: 112358}]
                    }
                });
        });

        it('Should skip fields with undefined values', () => {
            stageOneField.returns('x');
            stageThreeField.returns('z');

            const handleEvent = respondToEndOfSchema(assemblingContext);
            handleEvent({schemaName: 'the-schema'});
            const assemble = assemblingFunctions['the-schema'];
            const result = assemble('the sequence');

            expect(stageOneField).to.be.calledOnceWith('the sequence');
            expect(stageTwoField).to.be.calledOnceWith('the sequence');
            expect(stageThreeField).to.be.calledOnceWith('the sequence');

            expect(stack).to.be.empty;
            expect(result).to.be.deep.equal({object: {f1: 'x', f3: 'z'}});
        });
    });
});
