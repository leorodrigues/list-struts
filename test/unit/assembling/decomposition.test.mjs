import chai from 'chai';

import { IncompleteDefinitionError } from '../../../lib/errors.mjs';
import { ErrorTracker, ExecutionPointError } from '@leorodrigues/test-support';

const tracker = new ErrorTracker();

const { expect } = chai;

import {
    decomposeKey,
    decomposeType,
    decomposeDigest,
    decomposeOptions,
    decomposeReference,
    decomposeInheritance
} from '../../../lib/assembling/decomposition.mjs';

describe('lib/assembling/decomposition', () => {
    afterEach(() => tracker.printErrors());

    describe('#decomposeKey(definition)', () => {
        it('Should throw if there are not enough arguments.', () => {
            try {
                tracker.runAndTrack(() => decomposeKey([]));

                /* istanbul ignore next */
                throw new ExecutionPointError();
            } catch(error) {
                expect(error).to.be.instanceOf(IncompleteDefinitionError);
            }
        });
        it('Should return the second argument as undefined.', () => {
            const [fieldName, clashResolutioinStrategy] =
                decomposeKey(['child-field']);
            expect(fieldName).to.be.equal('child-field');
            expect(clashResolutioinStrategy).to.be.undefined;
        });
        it('Should return both arguments.', () => {
            const [fieldName, clashResolutioinStrategy] =
                decomposeKey(['child-field', 'keep-new']);
            expect(fieldName).to.be.equal('child-field');
            expect(clashResolutioinStrategy).to.be.equal('keep-new');
        });
    });

    describe('#decomposeType(definition)', () => {
        it('Should throw an error if the input is empty', () => {
            try {
                tracker.runAndTrack(() => decomposeType([]));

                /* istanbul ignore next */
                throw new ExecutionPointError();
            } catch(error) {
                expect(error).to.be.instanceOf(IncompleteDefinitionError);
            }
        });
        it('Should throw an error if an incomplete pattern is given', () => {
            try {
                tracker.runAndTrack(() =>
                    decomposeType(['string', ['pattern']]));

                /* istanbul ignore next */
                throw new ExecutionPointError();
            } catch(error) {
                expect(error).to.be.instanceOf(IncompleteDefinitionError);
            }
        });
        it('Should throw an error an ambiguous pattern is given', () => {
            try {
                tracker.runAndTrack(() =>
                    decomposeType(['string', ['pattern', 'x', 'y']]));

                /* istanbul ignore next */
                throw new ExecutionPointError();
            } catch(error) {
                expect(error).to.be.instanceOf(IncompleteDefinitionError);
            }
        });
        it('Should do nothing if the definition is "null" or "undefined"', () => {
            expect(decomposeType()).to.be.undefined;
            expect(decomposeType(null)).to.be.undefined;
        });
        it('Should return a simple type spec', () => {
            const [typeName, isArray, parameters] = decomposeType(['string']);
            expect(typeName).to.be.equal('string');
            expect(isArray).to.be.false;
            expect(parameters).to.be.instanceOf(Array).and.to.be.empty;
        });
        it('Should return an array type spec', () => {
            const [typeName, isArray, parameters] =
                decomposeType(['some-type', 'array']);
            expect(typeName).to.be.equal('some-type');
            expect(isArray).to.be.true;
            expect(parameters).to.be.instanceOf(Array).and.to.be.empty;
        });
        it('Should return a type spec with pattern', () => {
            const [typeName, isArray, parameters] =
                decomposeType(['some-type', ['pattern', '\\d']]);
            expect(typeName).to.be.equal('some-type');
            expect(isArray).to.be.false;
            expect(parameters).to.be.deep.equal(['\\d']);
        });
        it('Should return an array type spec with pattern', () => {
            const [typeName, isArray, parameters] =
                decomposeType(['some-type', 'array', ['pattern', '\\w']]);
            expect(typeName).to.be.equal('some-type');
            expect(isArray).to.be.true;
            expect(parameters).to.be.deep.equal(['\\w']);
        });
    });

    describe('#decomposeDigest(definition)', () => {
        it('Should throw an error if the input is empty', () => {
            try {
                tracker.runAndTrack(() => decomposeDigest([]));

                /* istanbul ignore next */
                throw new ExecutionPointError();
            } catch(error) {
                expect(error).to.be.instanceOf(IncompleteDefinitionError);
            }
        });
        it('Should throw if no field list is given', () => {
            try {
                tracker.runAndTrack(() => decomposeDigest(['sha1']));

                /* istanbul ignore next */
                throw new ExecutionPointError();
            } catch(error) {
                expect(error).to.be.instanceOf(IncompleteDefinitionError);
            }
        });
        it('Should do nothing if the definition is "null" or "undefined"', () => {
            expect(decomposeDigest()).to.be.undefined;
            expect(decomposeDigest(null)).to.be.undefined;
        });
        it('Should return the algorithm name and field list', () => {
            const [name, fieldList] = decomposeDigest(['md5', ['a'], ['b', 'x']]);
            expect(name).to.be.equal('md5');
            expect(fieldList).to.be.deep.equal([['a'], ['b', 'x']]);
        });
    });

    describe('#decomposeOptions(definition)', () => {
        it('Should return a "false tulpe" if the input is null, undefined or empty', () => {
            expect(decomposeOptions()).to.be.deep.equal([false, false]);
            expect(decomposeOptions(null)).to.be.deep.equal([false, false]);
            expect(decomposeOptions([])).to.be.deep.equal([false, false]);
        });
        it('Should return false/true for "whole-content" definition', () => {
            const [isOptional, isWholeContent] =
                decomposeOptions(['whole-content']);

            expect(isOptional).to.be.false;
            expect(isWholeContent).to.be.true;
        });
        it('Should return true/false for "optional" definition', () => {
            const [isOptional, isWholeContent] =
                decomposeOptions(['optional']);

            expect(isOptional).to.be.true;
            expect(isWholeContent).to.be.false;
        });
        it('Should return true/true for "optional, whole-content" definition', () => {
            const [isOptional, isWholeContent] =
                decomposeOptions(['optional', 'whole-content']);

            expect(isOptional).to.be.true;
            expect(isWholeContent).to.be.true;
        });
    });

    describe('#decomposeReference(definition)', () => {
        it('Should throw an error if the definition is empty', () => {
            try {
                tracker.runAndTrack(() => decomposeReference([]));

                /* istanbul ignore next */
                throw new ExecutionPointError();
            } catch(error) {
                expect(error).to.be.instanceOf(IncompleteDefinitionError);
            }
        });
        it('Should do nothing if the definition is "null" or "undefined"', () => {
            expect(decomposeReference()).to.be.undefined;
            expect(decomposeReference(null)).to.be.undefined;
        });
        it('Should return a "tuple" for a simple reference definition', () => {
            const [schemaName, isArray, isDetached] =
                decomposeReference(['child-schema']);

            expect(schemaName).to.be.deep.equal('child-schema');
            expect(isArray).to.be.false;
            expect(isDetached).to.be.false;
        });
        it('Should return a "tuple" based on an array reference definition', () => {
            const [schemaName, isArray, isDetached] =
                decomposeReference(['child-schema', 'array']);

            expect(schemaName).to.be.deep.equal('child-schema');
            expect(isArray).to.be.true;
            expect(isDetached).to.be.false;
        });
        it('Should return a "tuple" based on a detached reference definition', () => {
            const [schemaName, isArray, isDetached] =
                decomposeReference(['child-schema', 'detached']);

            expect(schemaName).to.be.deep.equal('child-schema');
            expect(isArray).to.be.false;
            expect(isDetached).to.be.true;
        });
        it('Should return a "tuple" based on a detached array reference definition', () => {
            const definition =
                ['child-schema', 'array', 'detached'];

            const [schemaName, isArray, isDetached] =
                decomposeReference(definition);

            expect(schemaName).to.be.deep.equal('child-schema');
            expect(isArray).to.be.true;
            expect(isDetached).to.be.true;
        });
    });

    describe('#decomposeInheritance(definition)', () => {
        it('Should throw an error if the definition is empty', () => {
            try {
                tracker.runAndTrack(() => decomposeInheritance([]));

                /* istanbul ignore next */
                throw new ExecutionPointError();
            } catch(error) {
                expect(error).to.be.instanceOf(IncompleteDefinitionError);
            }
        });
        it('Should do nothing if the definition is "null" or "undefined"', () => {
            expect(decomposeInheritance()).to.be.undefined;
            expect(decomposeInheritance(null)).to.be.undefined;
        });
        it('Should return an inheritance "tuple" with only the schema name', () => {
            expect(decomposeInheritance(['parent-schema']))
                .to.be.deep.equal(['parent-schema']);
        });
        it('Should return an inheritance "tuple" with schema and field names', () => {
            expect(decomposeInheritance(['parent-schema', 'parent-data']))
                .to.be.deep.equal(['parent-schema', 'parent-data']);
        });
    });
});
