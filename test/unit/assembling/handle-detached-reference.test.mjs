import chai from 'chai';
import sinon from 'sinon';
import sinonChai from 'sinon-chai';
import path from 'path';
import _ from 'lodash';
import { loadJson } from '@leorodrigues/test-support';
import { ExecutionPointError, ErrorTracker } from '@leorodrigues/test-support';
import { AttributeNotFoundError } from '../../../lib/errors.mjs';
import { handleDetachedReference } from
    '../../../lib/assembling/handle-detached-reference.mjs';

const { expect } = chai;

const tracker = new ErrorTracker();

chai.use(sinonChai);

const sandbox = sinon.createSandbox();
const assembleChild = sandbox.stub();

const stageTwoCalls = [];
const stack = [{
    stageTwoCalls,
    schemaName: 'parent-schema',
    attributeName: 'parent-attribute',
}];
const assemblingFunctions = {
    'child-schema': assembleChild
};
const wholeContentGraph = [];
const detachmentGraph = [];
const schemaKeys = {
    'child-schema': { fieldName: 'child-id' }
};
const context = {
    stack,
    schemaKeys,
    assemblingFunctions,
    wholeContentGraph,
    detachmentGraph
};

const dirname = import.meta.dirname;
const scenarios = loadJson(path.join(dirname, './handle-detached-reference.data.json'));

describe('lib/assembling/handle-detached-reference', () => {
    afterEach(() => {
        sandbox.reset();
        stageTwoCalls.shift();
        tracker.printErrors();
    });

    it('Yields a parse function that throws when the detachment field is missing', () => {
        const eventData = {
            'schemaName': 'child-schema',
            'isArray': false,
            'isOptional': false,
            'isWholeContent': false
        };

        handleDetachedReference(context, eventData);

        const current = { };
        const sequence = [['parent-attribute', 'child-id']];
        const instances = { };
        const ancestors = { shift: sinon.stub(), unshift: sinon.stub() };
        const [, assembleObject] = stack[0].stageTwoCalls[0];

        assembleChild.returns({object: { }});

        const assemblingContext = { ancestors, instances };

        try {
            tracker.runAndTrack(() => assembleObject(sequence, current, assemblingContext));

            /* istanbul ignore next */
            throw new ExecutionPointError();
        } catch(error) {
            expect(error).to.be.instanceOf(AttributeNotFoundError);
        }
    });

    it('Yields a parse function that piles instances up by schema name', () => {
        const eventData = {
            'schemaName': 'child-schema',
            'isArray': false,
            'isOptional': false,
            'isWholeContent': false
        };

        handleDetachedReference(context, eventData);

        const current = { };
        const sequence = [['parent-attribute', 'some-value']];
        const instances = { };
        const ancestors = { shift: sinon.stub(), unshift: sinon.stub() };
        const [, assembleObject] = stack[0].stageTwoCalls[0];

        assembleChild.onCall(0).returns({object: { 'child-id': '112358' }});
        assembleChild.onCall(1).returns({object: { 'child-id': '132134' }});

        const assemblingContext = { ancestors, instances };

        assembleObject(sequence, current, assemblingContext);
        assembleObject(sequence, current, assemblingContext);

        expect(instances).to.be.deep.equal({
            'child-schema': [
                { 'child-id': '112358' },
                { 'child-id': '132134' }
            ]
        });
    });

    it('Yields a parse function that returns undefined when the root is undefined', () => {
        const eventData = {
            'schemaName': 'child-schema',
            'isArray': false,
            'isOptional': false,
            'isWholeContent': false
        };

        handleDetachedReference(context, eventData);

        const current = { };
        const sequence = [['parent-attribute', 'child-id']];
        const instances = { };
        const ancestors = { shift: sinon.stub(), unshift: sinon.stub() };
        const [, assembleObject] = stack[0].stageTwoCalls[0];

        assembleChild.returns(undefined);

        const assemblingContext = { ancestors, instances };

        assembleObject(sequence, current, assemblingContext);

        expect(instances).to.be.deep.equal({ });
    });

    for (const {title, eventData, retValue, expectedResult} of scenarios) {
        it(title, () => {
            handleDetachedReference(context, eventData);

            const current = { };
            const sequence = [['parent-attribute', ['some-value']]];
            const instances = { };
            const ancestors = { shift: sinon.stub(), unshift: sinon.stub() };
            const [name, assembleObject] = stack[0].stageTwoCalls[0];

            const assemblingContext = { ancestors, instances };

            assembleChild.returns({object: retValue || { 'child-id': '112358' }});

            const result = assembleObject(sequence, current, assemblingContext);

            expect(result).to.be.deep.equal(expectedResult || '112358');

            expect(name).to.be.equal('parent-attribute');

            expect(ancestors.unshift)
                .to.be.calledOnceWith(sinon.match(a =>
                    _.isEqual(a, {object: current, schemaName: 'parent-schema'})));

            expect(ancestors.shift)
                .to.be.calledOnceWith();
        });
    }
});
