import chai from 'chai';
import sinon from 'sinon';
import sinonChai from 'sinon-chai';
import path from 'path';
import _ from 'lodash';
import { loadJson } from '@leorodrigues/test-support';
import { handleRegularReference }
    from '../../../lib/assembling/handle-regular-reference.mjs';

chai.use(sinonChai);

const { expect } = chai;

const sandbox = sinon.createSandbox();
const assemble = sandbox.stub();

const stageTwoCalls = [ ];
const stack = [{
    stageTwoCalls,
    schemaName: 'parent-schema',
    attributeName: 'parent-attribute',
}];
const assemblingFunctions = {
    'child-schema': assemble
};
const wholeContentGraph = [ ];

const context = { stack, assemblingFunctions, wholeContentGraph };

const dirname = import.meta.dirname;
const scenarios = loadJson(path.join(dirname, './handle-regular-reference.data.json'));

describe('lib/assembling/handle-regular-reference', () => {
    afterEach(() => {
        sandbox.reset();
        stageTwoCalls.shift();
    });

    for (const { title, eventData, assembledObject } of scenarios) {
        it(title, () => {

            const { isArray } = eventData;

            assemble.returns({ object: isArray ? assembledObject[0] : assembledObject });

            handleRegularReference(context, eventData);

            const current = { };
            const sequence = [['parent-attribute', 'some-value']];
            const ancestors = { shift: sinon.stub(), unshift: sinon.stub() };
            const [name, assembleObject] = stack[0].stageTwoCalls[0];

            const assemblingContext = { ancestors };

            const result = assembleObject(sequence, current, assemblingContext);

            expect(result).to.be.deep.equal(assembledObject);

            expect(name).to.be.equal('parent-attribute');

            expect(ancestors.unshift)
                .to.be.calledOnceWith(sinon.match(a =>
                    _.isEqual(a, { object: current, schemaName: 'parent-schema' })));

            expect(ancestors.shift)
                .to.be.calledOnceWith();
        });
    }
});
