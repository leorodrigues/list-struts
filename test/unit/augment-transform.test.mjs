import chai from 'chai';
import sinonChai from 'sinon-chai';
import sinon from 'sinon';
import { ErrorTracker, ExecutionPointError } from '@leorodrigues/test-support';
import { RequiredFieldMissingError } from '../../lib/errors.mjs';
import { augmentTransform } from '../../lib/transform.mjs';

const { expect } = chai;

chai.use(sinonChai);

const tracker = new ErrorTracker();

const sandbox = sinon.createSandbox();
const fakeTransform = sandbox.stub();

describe('lib/transform#augument-transform', () => {
    afterEach(() => {
        tracker.printErrors();
        sandbox.reset();
    });

    describe('In the context of a optional, simple field', () => {
        it('Yields a function that invokes the delegate if the value is defined', () => {
            fakeTransform.returns('fake-result');
            const augmentedCall = augmentTransform(fakeTransform, false, true);
            const result = augmentedCall('fake-value', 112358, 132134);
            expect(result).to.be.equal('fake-result');
            expect(fakeTransform)
                .to.be.calledOnceWithExactly('fake-value', 112358, 132134);
        });

        it('Yields a function that does not invoke the delegate if the value is undefined', () => {
            const augmentedCall = augmentTransform(fakeTransform, false, true);
            const result = augmentedCall(undefined, 112358, 132134);
            expect(result).to.be.undefined;
            expect(fakeTransform).to.not.be.called;
        });
    });

    describe('In the context of a required, simple field', () => {
        it('Yields a function that invokes the delegate if the value is defined', () => {
            fakeTransform.returns('fake-result');
            const augmentedCall = augmentTransform(fakeTransform, false, false);
            const result = augmentedCall('fake-value', 112358, 132134);
            expect(result).to.be.equal('fake-result');
            expect(fakeTransform)
                .to.be.calledOnceWithExactly('fake-value', 112358, 132134);
        });
    
        it('Yields a function that throws an error if the value is undefined', () => {
            const augmentedCall = augmentTransform(fakeTransform, false, false);
            try {
                tracker.runAndTrack(() =>
                    augmentedCall(undefined, 112358, 132134));

                /* istanbul ignore next */
                throw new ExecutionPointError();
            } catch (error) {
                expect(error).to.be.instanceOf(RequiredFieldMissingError);
                expect(fakeTransform).to.not.be.called;
            }
        });
    });

    describe('In the context of a optional, array field', () => {
        it('Yields a function that invokes the delegate for each element of the arg', () => {
            fakeTransform.returns('x');
            const augmentedCall = augmentTransform(fakeTransform, true, true);
            const result = augmentedCall([1, 2, 3], 112358, 132134);
            expect(result).to.be.deep.equal(['x', 'x', 'x']);
            expect(fakeTransform)
                .to.be.calledWithExactly(1, 112358, 132134);
            expect(fakeTransform)
                .to.be.calledWithExactly(2, 112358, 132134);
            expect(fakeTransform)
                .to.be.calledWithExactly(3, 112358, 132134);
        });
    
        it('Yields a function that does not invoke the delegate if the value is undefined', () => {
            const augmentedCall = augmentTransform(fakeTransform, true, true);
            const result = augmentedCall(undefined, 112358, 132134);
            expect(result).to.be.undefined;
            expect(fakeTransform).to.not.be.called;
        });
    });

    describe('In the context of a required, array field', () => {
        it('Yields a function that invokes the delegate for each element of the arg', () => {
            fakeTransform.returns('x');
            const augmentedCall = augmentTransform(fakeTransform, true, false);
            const result = augmentedCall([1, 2, 3], 112358, 132134);
            expect(result).to.be.deep.equal(['x', 'x', 'x']);
            expect(fakeTransform)
                .to.be.calledWithExactly(1, 112358, 132134);
            expect(fakeTransform)
                .to.be.calledWithExactly(2, 112358, 132134);
            expect(fakeTransform)
                .to.be.calledWithExactly(3, 112358, 132134);
        });
    
        it('Yields a function that throws an error if the value is undefined', () => {
            const augmentedCall = augmentTransform(fakeTransform, true, false);
            try {
                tracker.runAndTrack(() =>
                    augmentedCall(undefined, 112358, 132134));

                /* istanbul ignore next */
                throw new ExecutionPointError();
            } catch (error) {
                expect(error).to.be.instanceOf(RequiredFieldMissingError);
                expect(fakeTransform).to.not.be.called;
            }
        });
    });
});
