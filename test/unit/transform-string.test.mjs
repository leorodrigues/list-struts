import chai from 'chai';
import { ErrorTracker, ExecutionPointError } from '@leorodrigues/test-support';
import { transformString }  from '../../lib/transform.mjs';
import { DefinitionMismatchError } from '../../lib/errors.mjs';

const tracker = new ErrorTracker();

const { expect } = chai;

describe('lib/transform#transform-string', () => {
    afterEach(() => tracker.printErrors());

    it('Should refuse a string that does not match a pattern', () => {
        try {
            tracker.runAndTrack(() => transformString('abcd', 'xyz'));

            /* istanbul ignore next */
            throw new ExecutionPointError();
        } catch(error) {
            expect(error).to.be.instanceOf(DefinitionMismatchError);
        }
    });
    it('Should accept a string w/o pattern', () => {
        const result = transformString('abcd');
        expect(result).to.be.equal('abcd');
    });
    it('Should accept a string which matches the given pattern', () => {
        const result = transformString('abc', 'abc|xyz');
        expect(result).to.be.equal('abc');
    });
});
