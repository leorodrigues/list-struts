import fs from 'fs';

export function loadFile(path) {
    return new Promise((resolve, reject) => {
        fs.readFile(path, (error, data) => {
            if (error)
                reject(error);
            else
                resolve(data);
        });
    });
}
