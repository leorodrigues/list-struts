FROM node:10.14-alpine

EXPOSE 9229

ADD . /list-struts

RUN cd /list-struts && \
    if [ -n "$HTTP_PROXY" ]; then echo "proxy=$HTTP_PROXY" >> ~/.npmrc; fi && \
    if [ -n "$HTTPS_PROXY" ]; then echo "https.proxy=$HTTPS_PROXY" >> ~/.npmrc; fi && \
    if [ -n "$HTTP_PROXY$HTTPS_PROXY" ]; then echo "strict-ssl=false" >> ~/.npmrc; fi && \
    npm install && \
    if [ -f ~/.npmrc ]; then rm ~/.npmrc; fi

VOLUME /list-struts/node_modules

WORKDIR /list-struts

CMD ["npm", "keeptesting"]