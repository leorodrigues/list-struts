import fs from 'fs';
import path from 'path';

const dirname = import.meta.dirname;
const filePath = path.join(dirname, '..', 'package.json');
const packageContent = fs.readFileSync(filePath);
const packageObject = JSON.parse(packageContent);

const PATTERN = /(\d+\.\d+\.\d+)(\+(\d+))?/;

const [,version,,currentBuildNumber] = packageObject.version.match(PATTERN);

const nextBuildNumber = parseInt(currentBuildNumber || 0) + 1;

packageObject.version = `${version}+${nextBuildNumber}`;

fs.writeFile(filePath, JSON.stringify(packageObject, null, 2), e => {
    if (e) throw e;
});