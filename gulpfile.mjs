import gulp from 'gulp';
import gClean from 'gulp-clean';
import { makeGulpReadme, declareTasks } from '@leorodrigues/gulp-readme';

const gReadme = makeGulpReadme();

const BUILD_PATH = 'build';

const JS_SOURCES = [
    'lib/**/*.mjs'
];

const NOTES_SOURCES = [
    'docs/notes/foreword.md',
    'docs/notes/sample-usage.md',
    'docs/notes/syntax.md',
    'docs/notes/warnings.md'
];

const ALL_SOURCES = {
    jsSources: JS_SOURCES,
    notesSources: NOTES_SOURCES,
    refSources: ['docs/references.json']
};

const defaultDocsTask = declareTasks(
    'list-struts', gulp, gReadme, BUILD_PATH, ALL_SOURCES);

gulp.task('list-struts-docs', gulp.series([defaultDocsTask], function() {
    return gulp.src(`${BUILD_PATH}/README.md`)
        .pipe(gulp.dest('./'));
}));

gulp.task('list-struts-clean-docs', function() {
    return gulp.src(`${BUILD_PATH}/`, {read: false})
        .pipe(gClean({force: true}));
});

gulp.task('default', gulp.series(['list-struts-docs']));
