
# Syntax

Here is a - more or less - EBNF grammar that is accepted by the `parse` function:

```ebnf

List ::= '[', Item, { Item }, ']';

Item ::= String | Literal | Number | List;

String ::= '"', Character, { Character }, '"';

Character ::= ? all possible characters except '"' ?;

Literal ::= Letter | Digit | Symbol, { Letter | Digit | Symbol };

Number ::= Digit, { Digit }, [ '.', Digit, { Digit } ];

Letter ::= 'a'..'z' | 'A'..'Z' | '_';

Symbol ::= ':' | '?' | '*' | '!' | '@' | '#' | '$' | '%'
        | '&' | ';' | '.' | ',' | '/' | '\' | '|';

Digit := '0'..'9';

```