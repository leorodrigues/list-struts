
# Warnings

At the moment, the code is requiring a lot of revision and the test scenarios
may be improved covering new situations. Also, the implementations may need
performance optimizations.

The documentation on the modules is rather lacking, i.e. not all functions
are documented and only the main functions - the ones exported by the module -
are thoroughly documented.