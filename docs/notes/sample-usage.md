
# Install

```bash

$ npm i -SE bitbucket:leorodrigues/list-struts#v1.0.7

```

# Sample Usage

```javascript


// Import the minimal necessary functions from the library.

import { parse, makeObjectAssemblingFns } from '@leorodrigues/list-struts';

// Load the schemas and data files.

import fs from 'fs';
const schemaSource = fs.readFileSync('person.sch', 'utf-8');
const samplesSource = fs.readFileSync('people.list', 'utf-8');


// The most basic function of the library is parsing the text formated using
// a specific grammar and producing an abstraction called a sequence. It is
// nothing more than a list of lists.

const samples = parse(samplesSource);

const schema = parse(schemaSource);

console.log('>>> SCHEMA <<<');
console.log(JSON.stringify(schema));

console.log('>>> DATA <<<');
console.log(JSON.stringify(data));

// Afterwards, we use a process called assembling that is going to create
// a JSON object and possibly a map of detached instances based on the schema.

// The object assembling is carried by a set of functions dynamically created to
// cross the data sequence and build the object as they go:

const assemble = makeObjectAssemblingFns(schema);

// Then we apply the proper assemblage function to the data sequences.

const samplePeople = samples.map(s => assemble.person(s));

console.log(JSON.stringify(samplePeople, null, 4));

```

These are the input files used on the example above:

_person.sch_

```txt

[person
    [key id choose-new]
    [fields
        [id [digest sha1 [name] [age] [address]]]
        [name [type string]]
        [age [type number ]]
        [address [refer address detached]]]]

[address
    [key id]
    [fields
        [id [digest md5 [zip] [number]]]
        [zip [type number]]
        [number [type number]]
        [street [type string]]
        [state [type string [pattern "[A-Z]{2}"]]]
        [country [type string [pattern "[A-Z]{2}"]]]]]

```

_people.list_

```txt
[
    [name "Jack"]
    [age 39]
    [address
        [zip 112358]
        [number 19]
        [street "Nowere"]
        [state CA]
        [country US]
    ]
]
[
    [name "Martin"]
    [age 20]
    [address
        [zip 132134]
        [number 17]
        [street "Somewere"]
        [state SP]
        [country BR]
    ]
]
```

And this is how the final assembled object look like:

```json
[
    {
        "object": {
            "name": "Jack",
            "age": 39,
            "address": "9fcbd6226a076fb9aeb869a1e159a005",
            "id": "d8f86cdeb7d1a9e145fabad4992a17160547f897"
        },
        "detached": {
            "address": [
                {
                    "zip": 112358,
                    "number": 19,
                    "street": "Nowere",
                    "state": "CA",
                    "country": "US",
                    "id": "9fcbd6226a076fb9aeb869a1e159a005"
                }
            ]
        }
    },
    {
        "object": {
            "name": "Martin",
            "age": 20,
            "address": "a291a2f160697ab6cfc6e10b9f996731",
            "id": "aeadb00b5515558c697b977072cdd690bb06d538"
        },
        "detached": {
            "address": [
                {
                    "zip": 132134,
                    "number": 17,
                    "street": "Somewere",
                    "state": "SP",
                    "country": "BR",
                    "id": "a291a2f160697ab6cfc6e10b9f996731"
                }
            ]
        }
    }
]
```