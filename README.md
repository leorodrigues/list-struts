

# Foreword

This library implements a type of data structure based on lists, inspired by the syntax of [lisp](https://lisp-lang.org/).

### Install

```bash

$ npm i -SE bitbucket:leorodrigues/list-struts#v1.0.7

```

### Sample Usage

```javascript


// Import the minimal necessary functions from the library.

import { parse, makeObjectAssemblingFns } from '@leorodrigues/list-struts';

// Load the schemas and data files.

import fs from 'fs';
const schemaSource = fs.readFileSync('person.sch', 'utf-8');
const samplesSource = fs.readFileSync('people.list', 'utf-8');


// The most basic function of the library is parsing the text formated using
// a specific grammar and producing an abstraction called a sequence. It is
// nothing more than a list of lists.

const samples = parse(samplesSource);

const schema = parse(schemaSource);

console.log('>>> SCHEMA <<<');
console.log(JSON.stringify(schema));

console.log('>>> DATA <<<');
console.log(JSON.stringify(data));

// Afterwards, we use a process called assembling that is going to create
// a JSON object and possibly a map of detached instances based on the schema.

// The object assembling is carried by a set of functions dynamically created to
// cross the data sequence and build the object as they go:

const assemble = makeObjectAssemblingFns(schema);

// Then we apply the proper assemblage function to the data sequences.

const samplePeople = samples.map(s => assemble.person(s));

console.log(JSON.stringify(samplePeople, null, 4));

```

These are the input files used on the example above:

_person.sch_

```txt

[person
    [key id choose-new]
    [fields
        [id [digest sha1 [name] [age] [address]]]
        [name [type string]]
        [age [type number ]]
        [address [refer address detached]]]]

[address
    [key id]
    [fields
        [id [digest md5 [zip] [number]]]
        [zip [type number]]
        [number [type number]]
        [street [type string]]
        [state [type string [pattern "[A-Z]{2}"]]]
        [country [type string [pattern "[A-Z]{2}"]]]]]

```

_people.list_

```txt
[
    [name "Jack"]
    [age 39]
    [address
        [zip 112358]
        [number 19]
        [street "Nowere"]
        [state CA]
        [country US]
    ]
]
[
    [name "Martin"]
    [age 20]
    [address
        [zip 132134]
        [number 17]
        [street "Somewere"]
        [state SP]
        [country BR]
    ]
]
```

And this is how the final assembled object look like:

```json
[
    {
        "object": {
            "name": "Jack",
            "age": 39,
            "address": "9fcbd6226a076fb9aeb869a1e159a005",
            "id": "d8f86cdeb7d1a9e145fabad4992a17160547f897"
        },
        "detached": {
            "address": [
                {
                    "zip": 112358,
                    "number": 19,
                    "street": "Nowere",
                    "state": "CA",
                    "country": "US",
                    "id": "9fcbd6226a076fb9aeb869a1e159a005"
                }
            ]
        }
    },
    {
        "object": {
            "name": "Martin",
            "age": 20,
            "address": "a291a2f160697ab6cfc6e10b9f996731",
            "id": "aeadb00b5515558c697b977072cdd690bb06d538"
        },
        "detached": {
            "address": [
                {
                    "zip": 132134,
                    "number": 17,
                    "street": "Somewere",
                    "state": "SP",
                    "country": "BR",
                    "id": "a291a2f160697ab6cfc6e10b9f996731"
                }
            ]
        }
    }
]
```

### Syntax

Here is a - more or less - EBNF grammar that is accepted by the `parse` function:

```ebnf

List ::= '[', Item, { Item }, ']';

Item ::= String | Literal | Number | List;

String ::= '"', Character, { Character }, '"';

Character ::= ? all possible characters except '"' ?;

Literal ::= Letter | Digit | Symbol, { Letter | Digit | Symbol };

Number ::= Digit, { Digit }, [ '.', Digit, { Digit } ];

Letter ::= 'a'..'z' | 'A'..'Z' | '_';

Symbol ::= ':' | '?' | '*' | '!' | '@' | '#' | '$' | '%'
        | '&' | ';' | '.' | ',' | '/' | '\' | '|';

Digit := '0'..'9';

```

### Warnings

At the moment, the code is requiring a lot of revision and the test scenarios may be improved covering new situations. Also, the implementations may need performance optimizations.

The documentation on the modules is rather lacking, i.e. not all functions are documented and only the main functions - the ones exported by the module - are thoroughly documented.

# Api Reference

## Summary

 - [lib/errors](#module:lib/errors)
 - [lib/parse](#module:lib/parse)
 - [lib/assembling/collect-merging-function](#module:lib/assembling/collect-merging-function)
 - [lib/assembling](#module:lib/assembling)

## lib/errors
---

This module implements the various exception classes used throughout the package.

## Summary

 - [UnknownClashResolutionAlgorithmError](#module:lib/errors.UnknownClashResolutionAlgorithmError)
   - [constructor](#module:lib/errors.UnknownClashResolutionAlgorithmError#constructor)
 - [UnpackError](#module:lib/errors.UnpackError)
   - [constructor](#module:lib/errors.UnpackError#constructor)
 - [IllegalCycleError](#module:lib/errors.IllegalCycleError)
   - [constructor](#module:lib/errors.IllegalCycleError#constructor)
 - [RequiredFieldMissingError](#module:lib/errors.RequiredFieldMissingError)
 - [MissingSchemaError](#module:lib/errors.MissingSchemaError)
   - [constructor](#module:lib/errors.MissingSchemaError#constructor)
 - [AncestorReferenceError](#module:lib/errors.AncestorReferenceError)
   - [constructor](#module:lib/errors.AncestorReferenceError#constructor)
 - [IncompleteDefinitionError](#module:lib/errors.IncompleteDefinitionError)
   - [constructor](#module:lib/errors.IncompleteDefinitionError#constructor)
 - [AttributeNotFoundError](#module:lib/errors.AttributeNotFoundError)
   - [constructor](#module:lib/errors.AttributeNotFoundError#constructor)
 - [InvalidDigestNameError](#module:lib/errors.InvalidDigestNameError)
   - [constructor](#module:lib/errors.InvalidDigestNameError#constructor)
 - [DefinitionMismatchError](#module:lib/errors.DefinitionMismatchError)
   - [constructor](#module:lib/errors.DefinitionMismatchError#constructor)

## (_static_) UnknownClashResolutionAlgorithmError

Thrown by the clash resolution factory function if it doesn't recognize a given name.

### (_static_) constructor
---

Initializes an instance of "UnknownClashResolutionAlgorithmError".

**Parameters**

- **algorithmName** - _String_ - The name of a clash resolution algorithm recognized by the factory function.



## (_static_) UnpackError

Thrown by the 'unpack' function if it doesn't find the 'object' attribute of a supposed assembly result.

### (_static_) constructor
---

Creates a new instance of "UnpackError".

**Parameters**

- **message** - _String_ - The explanation for the exception.



## (_static_) IllegalCycleError

Thrown during the build phase of an assembling function. This exception indicates that the current schema being processed leads to an illegal reference cycle.

### (_static_) constructor
---

Creates a new instance of "IllegalCycleError".

**Parameters**

- **message** - _String_ - The explanation for the exception.



## (_static_) RequiredFieldMissingError

Thrown by the required field decorator function, during the object assembly phase.

## (_static_) MissingSchemaError

Thrown by the schema jump function, during the object assembly phase. It indicates that the schema currently being used to assemble a new object, reffers to a target schema that is not present in the given schema map.

### (_static_) constructor
---

Creates a new instance of "MissingSchemaError".

**Parameters**

- **schemaName** - _String_ - The name of the missing target schema.



## (_static_) AncestorReferenceError

Thrown by the data copy inheritance function, during the object assembly phase. It indicates that the schema currently being used to assemble a new object, asks to copy data from an ancestor whos schema is unknown.

### (_static_) constructor
---

Creates a new instance of "AncestorReferenceError".

**Parameters**

- **schemaName** - _String_ - The name of the missing target schema.



## (_static_) IncompleteDefinitionError

Thrown during the schema parse phase if for example a 'type' definition is incomplete.

### (_static_) constructor
---

Creates a new instance of "IncompleteDefinitionError".

**Parameters**

- **message** - _String_ - The cause of the error. If undefined, a default message is used.



## (_static_) AttributeNotFoundError

Thrown during the object assembly phase whenever an object lacks a specific attribute.

### (_static_) constructor
---

Creates a new instance of "AttributeNotFoundError".

**Parameters**

- **attributePath** - _String_ - The path used to reach the missing attribute through the acestry tree.



## (_static_) InvalidDigestNameError

Thrown during the schema parse phase if the name given on a 'digest' definition is not supported.

### (_static_) constructor
---

Creates a new instance of "InvalidDigestNameError".

**Parameters**

- **digestName** - _String_ - The missing algorithm name as given on the &#39;digest&#39; definition of some field.



## (_static_) DefinitionMismatchError

Thrown during the object assembly phase whenever an attribute fails to comply with a pattern. It may be a specific pattern defined via regex on a string field or the field may be of type `number`, but the field actual value does not comply with a number pattern (`\d+` or `\d+.\d+`).

### (_static_) constructor
---

Creates a new instance of "InvalidDigestNameError".

**Parameters**

- **value** - _any_ - The value that fails to comply with the given pattern.
- **definition** - _String_ - The pattern itself.
- **message** - _String_ - An optional custom message.



## lib/parse
---

This module implements the parsing of text strings into actual lists. Only one function is exported by this module.



### (__) module:lib/parse
---

Parses a javascript `string` into a list of lists in accordance with the syntax described in the documentation.

**Parameters**

- **text** - _String_ - A string with valid syntax.

**Returns**

 - **Array** - The list of lists.


## lib/assembling/collect-merging-function
---

This module instantiates a handler for the 'end of schema' event.



### (_inner_) mergeInstances
---

Merges all 'object assembly results'.

This function will delegate it's arguments to the `mergeParseResults` function along with the bound `schemaName`.

**Parameters**

- **assembledInstances** - _Array_ - A uniform list of &#39;assembled objects&#39;. Possibly empty.
- **mergedInstances** - _Object_ - A map of key-value pairs. Each key is the name of a schema and their corresponding values are lists of objects that have been assembled according to each of those schemas. This parameter is optional.

**Returns**

 - **Object** - The `mergedInstances` map given as input or a new one containing all the merged instances.


## lib/assembling
---

This module group implements all the algorithms necessary to transform a schema file into functions used to assemble and group json objects from list-struts.

The group is composed of several submodules separating the control algorithms from the action algorithms via the events API (`EventEmitter` class).

A single control algorithm is declared in `control` module. The `respond-to-*` and `collect-merging-function` modules, declare the various event handler functions. The remaining modules were all created in order to break down the complexity of the more "robust" handlers.

## Summary

 - [makeObjectAssemblingFns](#module:lib/assembling.makeObjectAssemblingFns)
 - [makeObjectAssemblingAndMergingFns](#module:lib/assembling.makeObjectAssemblingAndMergingFns)

### (_static_) makeObjectAssemblingFns
---

This function will produce an "object assemling function" for each schema in the given array.

**Parameters**

- **schemas** - _Array_ - An array of schemas in the form of parsed `list-struts`. May also be empty.

**Returns**

 - **Object** - Each key will be the name of a schema and each value will be the corresponding object assembly function. The object may also be empty, if the input schema array is empt.


### (_static_) makeObjectAssemblingAndMergingFns
---

This function will produce an "object assemling function" for each schema in the given array and will also produce a specific "result merging function" for each schema in the given array.

**Parameters**

- **schemas** - _Array_ - An array of schemas in the form of parsed `list-struts`. May also be empty.

**Returns**

 - **Array** - The value returned here is actually a tuple consisting of two objects. The first is exactly the same as the output from this functions counterpart (`makeObjectAssemblingFns`). The second is composed of keys made from the names of each schema and &amp;quot;result merging functions&amp;quot; as their corresponding values. Although the tuple is never empty, each object may be empty, if the given input is empty.


