import { isUndefinedOrEmpty } from './support.mjs';

import {
    DefinitionMismatchError,
    RequiredFieldMissingError
} from './errors.mjs';

/* istanbul ignore next */
export function transformRaw(value) {
    return value;
}

export function transformNumber(value) {
    value = value.toString();
    if (/^\d+$/.test(value))
        return parseInt(value);
    if (/^\d+\.\d+$/.test(value))
        return parseFloat(value);
    throw new DefinitionMismatchError(
        value, undefined, 'Value does not comply with number pattern.');
}

export function transformString(value, pattern) {
    const result = value.toString();
    if (pattern && !(new RegExp(pattern)).test(result))
        throw new DefinitionMismatchError(value, pattern);
    return result;
}

function makeArrayTransform(transform) {
    return function transformArray(value, ...parameters) {
        return value.map(v => transform(v, ...parameters));
    };
}

function makeRequiredTransform(transform) {
    return function transformRequiredField(value, ...parameters) {
        if (!isUndefinedOrEmpty(value))
            return transform(value, ...parameters);
        throw new RequiredFieldMissingError();
    };
}

function makeOptionalFieldTransform(transform) {
    return function transformOptionalField(value, ...parameters) {
        if (!isUndefinedOrEmpty(value))
            return transform(value, ...parameters);
    };
}

export function augmentTransform(delegate, isArray, isOptional) {
    const transformValue = isArray ? makeArrayTransform(delegate) : delegate;
    return isOptional
        ? makeOptionalFieldTransform(transformValue)
        : makeRequiredTransform(transformValue);
}
