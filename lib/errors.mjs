/**
 * This module implements the various exception classes used throughout the
 * package.
 * @module lib/errors
 * @author Teixeira, Leonardo Rodrigues <leorodriguesrj@gmail.com>
 */


/**
 * Thrown by the clash resolution factory function if it doesn't recognize
 * a given name.
 * @extends Error
 */
export class UnknownClashResolutionAlgorithmError extends Error {
    /**
     * Initializes an instance of "UnknownClashResolutionAlgorithmError".
     * @param {String} algorithmName The name of a clash resolution
     * algorithm recognized by the factory function.
     */
    constructor(algorithmName) {
        super(`Unknown clash resolution algorithm: ${algorithmName}`);
    }
}

/**
 * Thrown by the 'unpack' function if it doesn't find the 'object' attribute
 * of a supposed assembly result.
 * @extends Error
 */
export class UnpackError extends Error {
    /**
     * Creates a new instance of "UnpackError".
     * @param {String} message The explanation for the exception.
     */
    constructor(message) {
        super(message);
    }
}

/**
 * Thrown during the build phase of an assembling function. This exception
 * indicates that the current schema being processed leads to an illegal
 * reference cycle.
 * @extends Error
 */
export class IllegalCycleError extends Error {
    /**
     * Creates a new instance of "IllegalCycleError".
     * @param {String} message The explanation for the exception.
     */
    constructor(message) {
        super(message);
    }
}

/**
 * Thrown by the required field decorator function, during the object
 * assembly phase.
 * @extends Error
 */
export class RequiredFieldMissingError extends Error {
    /**
     * Creates a new instance of "RequiredFieldMissingError".
     */
    constructor() {
        super('A required field is missing.');
    }
}

/**
 * Thrown by the schema jump function, during the object assembly phase. It
 * indicates that the schema currently being used to assemble a new object,
 * reffers to a target schema that is not present in the given schema map.
 * @extends Error
 */
export class MissingSchemaError extends Error {
    /**
     * Creates a new instance of "MissingSchemaError".
     * @param {String} schemaName The name of the missing target schema.
     */
    constructor(schemaName) {
        super(`Schema '${schemaName}' doesn't exist.`);
    }
}

/**
 * Thrown by the data copy inheritance function, during the object assembly
 * phase. It indicates that the schema currently being used to assemble a
 * new object, asks to copy data from an ancestor whos schema is unknown.
 * @extends Error
 */
export class AncestorReferenceError extends Error {
    /**
     * Creates a new instance of "AncestorReferenceError".
     * @param {String} schemaName The name of the missing target schema.
     */
    constructor(schemaName) {
        super(`Schema '${schemaName}' not found in ancestry sequence.`);
    }
}


/**
 * Thrown during the schema parse phase if for example a 'type' definition
 * is incomplete.
 * @extends Error
 */
export class IncompleteDefinitionError extends Error {
    /**
     * Creates a new instance of "IncompleteDefinitionError".
     * @param {String} message The cause of the error. If undefined,
     * a default message is used.
     */
    constructor(message) {
        super(message || 'An incomplete definition was found.');
    }
}

/**
 * Thrown during the object assembly phase whenever an object lacks a specific
 * attribute.
 * @extends Error
 */
export class AttributeNotFoundError extends Error {
    /**
     * Creates a new instance of "AttributeNotFoundError".
     * @param {String} attributePath The path used to reach the missing
     * attribute through the acestry tree.
     */
    constructor(attributePath) {
        super(`Attribute '${attributePath}' was not found.`);
    }
}

/**
 * Thrown during the schema parse phase if the name given on a 'digest'
 * definition is not supported.
 * @extends Error
 */
export class InvalidDigestNameError extends Error {
    /**
     * Creates a new instance of "InvalidDigestNameError".
     * @param {String} digestName The missing algorithm name as given on the
     * 'digest' definition of some field.
     */
    constructor(digestName) {
        super(`Digest algorithm '${digestName}' is not available.`);
    }
}

/**
 * Thrown during the object assembly phase whenever an attribute fails to
 * comply with a pattern. It may be a specific pattern defined via regex on a
 * string field or the field may be of type `number`, but the field actual value
 * does not comply with a number pattern (`\d+` or `\d+.\d+`).
 * @extends Error
 */
export class DefinitionMismatchError extends Error {
    /**
     * Creates a new instance of "InvalidDigestNameError".
     * @param {any} value The value that fails to comply with the given pattern.
     * @param {String} definition The pattern itself.
     * @param {String} message An optional custom message.
     */
    constructor(value, definition, message = 'Definition mismatch.') {
        super(message);
        this.value = value;
        this.definition = definition;
    }
}

export class ObjectAssemblageError extends Error {
    constructor(attributeName, cause) {
        const rootCause = cause.rootCause || cause.message;
        const trail = computeAssemblageErrorTrail(attributeName, cause.trail);
        const detail = `'${trail.join('.')}': ${rootCause}`;
        const message = `Trying to assemble path ${detail}`;

        super(message);
        this.trail = trail;
        this.rootCause = rootCause;

    }
}

function computeAssemblageErrorTrail(name, trail) {
    if (trail === undefined) return [name];
    return [name, ...trail];
}
