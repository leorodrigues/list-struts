/**
 * This module implements the parsing of text strings into actual lists.
 * Only one function is exported by this module.
 * @module lib/parse
 * @author Teixeira, Leonardo Rodrigues <leorodriguesrj@gmail.com>
 */

function applyForEachChar(text, action) {
    for (let i = 0; i < text.length; i++) {
        const skipPosition = action(i, text.charAt(i), text);
        if (skipPosition) i = skipPosition;
    }
}

function makeEscapeHandler(next) {
    return function (index, ch, text) {
        if (ch === '\\')
            return index + 1;
        return next(index, ch, text);
    };
}

function makeQuoteHandler(action, next) {
    let readingString = false;
    return function (index, ch, text) {
        if (ch === '"') {
            const result = action(index, ch, text);
            readingString = !readingString;
            return result;
        }
        if (!readingString)
            return next(index, ch, text);
    };
}

function fastForwardSpaceChars(start, text) {
    let i = start;
    while (text.charAt(i + 1) === ' ') i++;
    return i;
}

function makeSpaceHandler(action, next) {
    return function (index, ch, text) {
        if (ch === ' ') {
            const h = index;
            index = fastForwardSpaceChars(index, text);
            action([h, index], ch, text);
            return index;
        }
        next(index, ch, text);
    };
}

function makeOpeningHandler(action, next) {
    return function(index, ch, text) {
        if (ch === '[')
            action(index, ch, text);
        next(index, ch, text);
    };
}

function makeClosingHandler(action) {
    return function(index, ch, text) {
        if (ch === ']')
            action(index, ch, text);
    };
}

function confirmTrackIsInBalance(track, balance) {
    if (balance > 0)
        throw new SyntaxError(`"]" expected at ${track.pop()[1]}`);
}

function confirmStringIsFinished(readingString) {
    if (readingString)
        throw new SyntaxError('Unterminated string');
}

function trackTokenVariation(text) {
    const track = [];
    let readingString = false;
    let balance = 0;

    const handler = makeQuoteHandler(function(index) {
        track.push(['q', index]);
        readingString = !readingString;
    }, makeSpaceHandler(function(index) {
        track.push(['s', index]);
    }, makeOpeningHandler(function(index) {
        track.push(['o', index]);
        balance++;
    }, makeClosingHandler(function(index) {
        track.push(['c', index]);
        balance--;
    }))));

    applyForEachChar(text, makeEscapeHandler(handler));

    confirmTrackIsInBalance(track, balance);
    confirmStringIsFinished(readingString);

    return track;
}

function followingTrack(track, action) {
    const workingTrack = [...track];
    while (workingTrack.length > 0) {
        const [mark, index] = workingTrack.shift();
        action(mark, index);
    }
}

function makeSlicingFunctions() {
    let lastIndex = 0;

    function slice(index, {expression, text}) {
        if (lastIndex < index)
            expression.push(text.substring(lastIndex, index));
        lastIndex = index + 1;
    }

    return {
        's': (index, context) => {
            const [startOfSpaces, endOfSpaces] = index;
            slice(startOfSpaces, context);
            lastIndex = endOfSpaces + 1;
        },
        'o': slice,
        'c': slice,
        'q': slice
    };
}

function makeStackingFunctions() {
    const expressionStack = [];
    return {
        'o': (context) => {
            const next = [];
            context.expression.push(next);
            expressionStack.push(context.expression);
            context.expression = next;
        },
        'c': (context) => {
            context.expression = expressionStack.pop();
        }
    };
}

/**
 * Parses a javascript `string` into a list of lists in accordance with the
 * syntax described in the documentation.
 * @param {String} text A string with valid syntax.
 * @return {Array} The list of lists.
 */
export default function parse(text) {
    text = text.replace(/[\n\r]/gm, '');
    const result = [];
    const context = {text, expression: result};
    const track = trackTokenVariation(text);
    const slicing = makeSlicingFunctions();
    const stacking = makeStackingFunctions();

    followingTrack(track, (mark, index) => {
        const sliceText = slicing[mark];
        sliceText(index, context);
        if (mark in stacking) {
            const manageStack = stacking[mark];
            manageStack(context);
        }
    });

    return result;
}
