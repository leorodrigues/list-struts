import { selectClashResolution, makeFindClashFn } from './clash-resolution.mjs';

export function respondToKey(context) {
    const {stack, schemaKeys} = context;
    return function handleKey(eventData) {
        const {schemaName} = stack[0];
        const {fieldName, clashResolutionId = 'keep-old'} = eventData;
        const findClashing = makeFindClashFn(fieldName);
        const resolveClash = selectClashResolution(clashResolutionId);
        const data = {fieldName, clashResolutionId, resolveClash, findClashing};
        schemaKeys[schemaName] = data;
    };
}
