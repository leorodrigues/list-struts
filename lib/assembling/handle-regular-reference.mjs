import { augmentTransform } from '../transform.mjs';
import { jumpToSchema, getFieldContent } from './reference-operations.mjs';
import { updateDependencyGraph } from './safeguard.mjs';

function makeReferenceAttributeAssemblyFunction(
    attributeName,
    parameters,
    jumpTransform,
    isWholeContent,
    currentSchemaName) {

    function assembleAttributeFromReference(sequence, current, context) {
        const {ancestors} = context;
        const value = getFieldContent(isWholeContent, sequence, attributeName);
        ancestors.unshift({object: current, schemaName: currentSchemaName});
        const child = jumpTransform(value, context, ...parameters);
        ancestors.shift();
        return child;
    }

    return [attributeName, assembleAttributeFromReference];
}

export function handleRegularReference(context, eventData) {
    const { stack, assemblingFunctions, wholeContentGraph } = context;
    const currentSchemaName = stack[0].schemaName;
    const { attributeName, stageTwoCalls } = stack[0];
    const { schemaName, isArray, isOptional } = eventData;
    const { isWholeContent } = eventData;
    const parameters = [schemaName, assemblingFunctions];
    const delegate = jumpToSchema;
    const transform = augmentTransform(delegate, isArray, isOptional);

    updateDependencyGraph(
        wholeContentGraph, schemaName, isWholeContent);

    stageTwoCalls.push(makeReferenceAttributeAssemblyFunction(
        attributeName,
        parameters,
        transform,
        isWholeContent,
        currentSchemaName));
}
