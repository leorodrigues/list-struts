import { AttributeNotFoundError } from '../errors.mjs';

function expandFieldItem(item) {
    const reminder = [...item];
    return [reminder.shift(), reminder.shift()];
}

function wrapCollectAttribute(attributeName, ignoreMissing) {
    return function collectAttribute(object) {
        if (object === undefined) return;
        if (attributeName in object) return object[attributeName];
        if (ignoreMissing) return;
        throw new AttributeNotFoundError(attributeName);
    };
}

function wrapCollectSubAttribute(
    attributeName, subAttributeName, ignoreMissing
) {
    const collect = wrapCollectAttribute(attributeName, ignoreMissing);
    return function collectSubAttribute(object) {
        const value = collect(object);
        if (value === undefined) return;
        if (subAttributeName in value) return value[subAttributeName];
        if (ignoreMissing) return;
        throw new AttributeNotFoundError(
            `${attributeName}.${subAttributeName}`);
    };
}

export function makeCollectFieldValuesFunction(
    fieldList, ignoreMissing = false
) {
    const calls = fieldList.map(f => {
        const [attrName, subAttrName] = expandFieldItem(f);
        return subAttrName === undefined
            ? wrapCollectAttribute(attrName, ignoreMissing)
            : wrapCollectSubAttribute(attrName, subAttrName, ignoreMissing);
    });

    return function collectFields(object) {
        return calls.map(c => c(object));
    };
}
