
import { selectDigestImplementation } from './digest-operations.mjs';
import { makeCollectFieldValuesFunction } from './attribute-collections.mjs';

function makeDigestAssemblyFunction(attributeName, algorithm, fieldList) {
    const digest = selectDigestImplementation(algorithm);
    const collectFieldValues = makeCollectFieldValuesFunction(fieldList, true);
    function assembleDigest(sequence, current) {
        const hashes = collectFieldValues(current).map(digest);
        return digest(hashes.join(''));
    }
    return [attributeName, assembleDigest];
}

export function respondToDigest(context) {
    const { stack } = context;
    return function newDigest(eventData) {
        const { stageThreeCalls, attributeName } = stack[0];
        const { algorithm, fieldList } = eventData;
        stageThreeCalls.push(makeDigestAssemblyFunction(
            attributeName, algorithm, fieldList));
    };
}
