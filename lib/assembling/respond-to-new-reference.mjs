import { handleRegularReference } from './handle-regular-reference.mjs';
import { handleDetachedReference } from './handle-detached-reference.mjs';

export function respondToNewReference(context) {
    return function newReference(eventData) {
        if (eventData.isDetached)
            handleDetachedReference(context, eventData);
        else
            handleRegularReference(context, eventData);
    };
}
