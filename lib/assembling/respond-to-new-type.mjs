import { getField } from '../support.mjs';
import {
    augmentTransform,
    transformRaw,
    transformNumber,
    transformString
} from '../transform.mjs';

const TRANSFORM_TABLE = {
    'raw': transformRaw,
    'string': transformString,
    'number': transformNumber
};

function makePrimitiveAttributeAssemblyFunction(
    name, parameters, transform, isWholeContent) {
    function assemblePrimitive(sequence) {
        const value = isWholeContent ? sequence : getField(name, sequence);
        return transform(value, ...parameters);
    }
    return [name, assemblePrimitive];
}

export function respondToNewType({stack}) {
    return function newType(eventData) {
        const {attributeName, stageOneCalls} = stack[0];
        const {typeName, isArray, isOptional} = eventData;
        const {isWholeContent, parameters} = eventData;
        const delegate = TRANSFORM_TABLE[typeName];
        const transform = augmentTransform(delegate, isArray, isOptional);

        stageOneCalls.push(makePrimitiveAttributeAssemblyFunction(
            attributeName, parameters, transform, isWholeContent));
    };
}
