export function respondToNewSchema(context) {
    const {stack, wholeContentGraph, detachmentGraph} = context;
    return function newSchema({schemaName}) {
        stack.unshift({
            stageOneCalls: [],
            stageTwoCalls: [],
            stageThreeCalls: [],
            schemaName
        });
        wholeContentGraph.unshift({name: schemaName, children: []});
        detachmentGraph.unshift({name: schemaName, children: []});
    };
}
