import { MissingSchemaError } from '../errors.mjs';
import { getField, unpack } from '../support.mjs';

export function jumpToSchema(value, context, schemaName, knownSchemas) {
    if (schemaName in knownSchemas)
        return unpack(knownSchemas[schemaName](value, context));
    throw new MissingSchemaError(schemaName);
}

export function getFieldContent(isWholeContent, sequence, attributeName) {
    return isWholeContent ? sequence : getField(attributeName, sequence);
}
