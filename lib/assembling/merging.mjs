
import { selectClashResolution } from './clash-resolution.mjs';

function getCollection(map, key) {
    return map[key] || (map[key] = []);
}

function *iterate(schemaName, parseResults) {
    for (const r of parseResults) {
        yield [schemaName, r.object];
        if (!r.detached) continue;
        for (const k in r.detached)
            for (const o of r.detached[k])
                yield [k, o];
    }
}

export function mergeParseResults(
    schemaName,
    assembledInstances,
    mergedObjects = {},
    resolveClash = selectClashResolution('choose-new'),
    findClashing = () => undefined) {

    for (const [name, object] of iterate(schemaName, assembledInstances)) {
        const objs = getCollection(mergedObjects, name);
        const index = findClashing(objs, object);
        if (index >= 0)
            objs[index] = resolveClash(objs[index], object);
        else
            objs.push(object);
    }

    return mergedObjects;
}
