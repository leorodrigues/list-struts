import { bySchemaAttribute } from '../support.mjs';
import { AncestorReferenceError } from '../errors.mjs';

function safeGetAttribute(object, attributeName) {
    if (attributeName in object)
        return object[attributeName];
}

function makeDataCopyAssemblyFunction(name, targetSchema, targetAttribute) {
    function assembleDataCopy(sequence, current, {ancestors}) {
        const ancestor = ancestors.find(bySchemaAttribute(targetSchema));
        if (!ancestor)
            throw new AncestorReferenceError(targetSchema);
        return safeGetAttribute(ancestor.object, targetAttribute);
    }
    return [name, assembleDataCopy];
}

export function respondToInheritance(context) {
    const {stack} = context;
    return function newInheritance(eventData) {
        const {stageOneCalls, attributeName} = stack[0];
        const {targetSchema, targetAttribute = attributeName} = eventData;

        stageOneCalls.push(makeDataCopyAssemblyFunction(
            attributeName, targetSchema, targetAttribute));
    };
}
