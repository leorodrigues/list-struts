import { augmentTransform } from '../transform.mjs';
import { AttributeNotFoundError } from '../errors.mjs';
import { getFieldContent, jumpToSchema } from './reference-operations.mjs';
import { updateDependencyGraph } from './safeguard.mjs';

function saveInstance(object, context, schemaName) {
    if (object === undefined) return;
    const { instances } = context;
    if (schemaName in instances)
        instances[schemaName].push(object);
    else
        instances[schemaName] = [object];
}

function makeSaveManyInstances(delegate) {
    return function saveMany(array, context, schemaName) {
        return array.map(o => delegate(o, context, schemaName));
    };
}

function augmentSaveValue(delegate, isArray) {
    return isArray ? makeSaveManyInstances(delegate) : delegate;
}

function getOrThrow(detachmentField, object) {
    if (object === undefined) return;
    if (detachmentField in object)
        return object[detachmentField];
    throw new AttributeNotFoundError(detachmentField);
}

function makeGetManyOrThrow(delegate) {
    return function getManyOrThrow(detachmentField, valueArray) {
        return valueArray.map(v => delegate(detachmentField, v));
    };
}

function augmentGetField(delegate, isArray) {
    return isArray ? makeGetManyOrThrow(delegate) : delegate;
}

function makeDetachedReferenceAttributeAssemblyFunction(
    attributeName,
    parameters,
    jumpTransform,
    isWholeContent,
    currentSchemaName,
    schemaKeys,
    getField,
    saveValue) {
    const [referenceSchemaName] = parameters;

    function assembleAttributeFromDetachedReference(
        sequence, current, context
    ) {
        const detachmentField = schemaKeys[referenceSchemaName].fieldName;
        const { ancestors } = context;
        const value = getFieldContent(isWholeContent, sequence, attributeName);
        ancestors.unshift({ object: current, schemaName: currentSchemaName });
        const child = jumpTransform(value, context, ...parameters);
        ancestors.shift();
        saveValue(child, context, referenceSchemaName);
        return getField(detachmentField, child);
    }

    return [attributeName, assembleAttributeFromDetachedReference];
}

export function handleDetachedReference(context, eventData) {
    const { stack, assemblingFunctions, schemaKeys } = context;
    const { wholeContentGraph, detachmentGraph } = context;
    const currentSchemaName = stack[0].schemaName;
    const { attributeName, stageTwoCalls } = stack[0];
    const { schemaName, isArray, isOptional } = eventData;
    const { isWholeContent } = eventData;
    const parameters = [schemaName, assemblingFunctions];
    const transform = augmentTransform(jumpToSchema, isArray, isOptional);
    const getField = augmentGetField(getOrThrow, isArray);
    const saveValue = augmentSaveValue(saveInstance, isArray);

    updateDependencyGraph(
        wholeContentGraph, schemaName, isWholeContent);

    updateDependencyGraph(
        detachmentGraph, schemaName, true);

    const f = makeDetachedReferenceAttributeAssemblyFunction(
        attributeName,
        parameters,
        transform,
        isWholeContent,
        currentSchemaName,
        schemaKeys,
        getField,
        saveValue);

    stageTwoCalls.push(f);
}
