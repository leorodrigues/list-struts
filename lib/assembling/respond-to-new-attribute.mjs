export function respondToNewAttribute({stack}) {
    return function newAttribute({attributeName}) {
        stack[0].attributeName = attributeName;
    };
}
