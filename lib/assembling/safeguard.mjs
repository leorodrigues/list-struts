import { IllegalCycleError } from '../errors.mjs';

function contains(array, object) {
    return array.indexOf(object) > -1;
}

function byNameField(name) {
    return e => e.name === name;
}

function getCycle(tree, graph, track = [], visited = []) {
    const {name, children} = tree;

    if (contains(track, name))
        return [name, ...track];
    track.unshift(name);

    if (contains(visited, tree))
        return null;
    visited.push(tree);

    for (const c of children) {
        const nextTree = graph.find(byNameField(c.name));
        if (nextTree) {
            const cycle = getCycle(nextTree, graph, track, visited);
            if (cycle)
                return cycle;
        }
    }
    track.shift();
    return null;
}

export function confirmSafeReferenceGraph(referenceGraph, kind) {
    for (const tree of referenceGraph) {
        const cycle = getCycle(tree, referenceGraph);
        if (cycle)
            throw new IllegalCycleError(
                `Cycle of kind '${kind}' detected: '${cycle.join(' > ')}'.`);
    }
}

export function updateDependencyGraph(graph, schemaName, state) {
    if (!graph) return;
    if (state && graph.length > 0)
        if (!graph[0].children.some(byNameField(schemaName)))
            graph[0].children.push({name: schemaName});
}
