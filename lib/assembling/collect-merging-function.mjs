/**
 * This module instantiates a handler for the 'end of schema' event.
 * @module lib/assembling/collect-merging-function
 * @author Teixeira, Leonardo Rodrigues <leorodriguesrj@gmail.com>
 */

import { mergeParseResults } from './merging.mjs';

export function makeMergeInstancesFn(schemaName, resolveClash, findClashing) {
    /**
     * Merges all 'object assembly results'.
     *
     * This function will delegate it's arguments to the `mergeParseResults`
     * function along with the bound `schemaName`.
     *
     * @param {Array} assembledInstances A uniform list of 'assembled objects'.
     * Possibly empty.
     * @param {Object} mergedInstances A map of key-value pairs. Each key is
     * the name of a schema and their corresponding values are lists of objects
     * that have been assembled according to each of those schemas. This
     * parameter is optional.
     * @returns {Object} The `mergedInstances` map given as input or a new one
     * containing all the merged instances.
     */
    function mergeInstances(assembledInstances, mergedInstances = {}) {
        return mergeParseResults(
            schemaName,
            assembledInstances,
            mergedInstances,
            resolveClash,
            findClashing);
    }
    return mergeInstances;
}

export function collectMergingFunction(context) {
    const { mergingFunctions, schemaKeys } = context;
    return function finishMergingFunction({ schemaName }) {
        const  { resolveClash, findClashing } = schemaKeys[schemaName];
        mergingFunctions[schemaName] =
            makeMergeInstancesFn(schemaName, resolveClash, findClashing);
    };
}
