import {
    decomposeKey,
    decomposeType,
    decomposeDigest,
    decomposeOptions,
    decomposeReference,
    decomposeInheritance
} from './decomposition.mjs';

import { getField } from '../support.mjs';
import { IncompleteDefinitionError } from '../errors.mjs';
import EventEmitter from 'events';

const MSG_ERR_FIELD_LIST_MISSING = 'Field list is missing.';

class SchemaWalkEvents extends EventEmitter {}

function walkType(type, options, events) {
    const [typeName, isArray, parameters] = decomposeType(type);
    const [isOptional, isWholeContent] = decomposeOptions(options);
    const data = {typeName, isArray, isOptional, isWholeContent, parameters};
    events.emit('foundType', data);
}

function walkReference(reference, options, events) {
    const [schemaName, isArray, isDetached] = decomposeReference(reference);
    const [isOptional, isWholeContent] = decomposeOptions(options);
    const data = {
        schemaName, isArray, isOptional, isWholeContent, isDetached
    };
    events.emit('foundReference', data);
}

function walkInheritance(inheritance, events) {
    const [targetSchema, targetAttribute] = decomposeInheritance(inheritance);
    const data = {targetSchema, targetAttribute};
    events.emit('foundInheritance', data);
}

function walkDigest(digest, events) {
    const [algorithm, fieldList] = decomposeDigest(digest);
    events.emit('foundDigest', {algorithm, fieldList});
}

function walkAttribute(attribute, events) {
    const attributeName = attribute[0];
    const options = getField('options', attribute);
    const reference = getField('refer', attribute);
    const type = getField('type', attribute);
    const inheritance = getField('inherit', attribute);
    const digest = getField('digest', attribute);
    events.emit('foundAttribute', {attributeName});

    if (digest) {
        walkDigest(digest, events);
    } else if (inheritance) {
        walkInheritance(inheritance, events);
    } else if (reference) {
        walkReference(reference, options, events);
    } else {
        walkType(type, options, events);
    }
}

function walkKey(keySpec, events) {
    if (!keySpec) return;
    const [fieldName, clashResolutionId] = decomposeKey(keySpec);
    events.emit('foundKey', {fieldName, clashResolutionId});
}

function walkSchema(schema, events) {
    const schemaName = schema[0];
    events.emit('foundSchema', {schemaName});
    const fieldList = getField('fields', schema);
    if (!fieldList)
        throw new IncompleteDefinitionError(MSG_ERR_FIELD_LIST_MISSING);
    walkKey(getField('key', schema), events);
    for (const attribute of fieldList)
        walkAttribute(attribute, events);
    events.emit('endOfSchema', {schemaName});
}

export function walkSchemas(schemas, bindEvents) {
    const events = new SchemaWalkEvents();
    bindEvents(events);
    for (const schema of schemas)
        walkSchema(schema, events);
}
