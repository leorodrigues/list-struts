import { UnknownClashResolutionAlgorithmError } from '../errors.mjs';

function chooseNew(oldObject, newObject) {
    return newObject;
}

function keepOld(oldObject) {
    return oldObject;
}

export function makeFindClashFn(attributeName) {
    return function findClashing(array, object) {
        const value = object[attributeName];
        return array.findIndex(o => o[attributeName] === value);
    };
}

export function selectClashResolution(id) {
    if (id === 'keep-old')
        return keepOld;
    if (id === 'choose-new')
        return chooseNew;
    throw new UnknownClashResolutionAlgorithmError(id);
}
