/**
 * This module group implements all the algorithms necessary to transform a
 * schema file into functions used to assemble and group json objects from
 * list-struts.
 *
 * The group is composed of several submodules separating the control
 * algorithms from the action algorithms via the events API (`EventEmitter`
 * class).
 *
 * A single control algorithm is declared in `control` module. The
 * `respond-to-*` and `collect-merging-function` modules, declare the various
 * event handler functions. The remaining modules were all created in order to
 * break down the complexity of the more "robust" handlers.
 * @module lib/assembling
 * @author Teixeira, Leonardo Rodrigues <leorodriguesrj@gmail.com>
 */

import { respondToNewSchema } from './respond-to-new-schema.mjs';

import { respondToNewAttribute } from './respond-to-new-attribute.mjs';

import { respondToDigest } from './respond-to-digest.mjs';

import { respondToNewReference } from './respond-to-new-reference.mjs';

import { respondToInheritance } from './respond-to-inheritance.mjs';

import { respondToEndOfSchema } from './respond-to-end-of-schema.mjs';

import { respondToNewType } from './respond-to-new-type.mjs';

import { respondToKey } from './respond-to-key.mjs';

import { collectMergingFunction } from './collect-merging-function.mjs';

import { walkSchemas } from './control.mjs';

function makeContext() {
    return {
        stack: [ ],
        assemblingFunctions: { },
        schemaKeys: { },
        wholeContentGraph: [ ],
        detachmentGraph: [ ]
    };
}

function registerForAssemblingFnConstruction(events, context) {
    events.on('foundSchema', respondToNewSchema(context));
    events.on('foundKey', respondToKey(context));
    events.on('foundAttribute', respondToNewAttribute(context));
    events.on('foundDigest', respondToDigest(context));
    events.on('foundReference', respondToNewReference(context));
    events.on('foundInheritance', respondToInheritance(context));
    events.on('foundType', respondToNewType(context));
    events.on('endOfSchema', respondToEndOfSchema(context));
}

function registerForMergingFnConstruction(events, context) {
    context.mergingFunctions = { };
    events.on('endOfSchema', collectMergingFunction(context));
}

/**
 * This function will produce an "object assemling function" for each schema in
 * the given array.
 * @param {Array} schemas An array of schemas in the form of parsed
 * `list-struts`. May also be empty.
 * @returns {Object} Each key will be the name of a schema and each value will
 * be the corresponding object assembly function. The object may also be empty,
 * if the input schema array is empt.
 */
export function makeObjectAssemblingFns(schemas) {
    const context = makeContext();
    walkSchemas(schemas, e => registerForAssemblingFnConstruction(e, context));
    return context.assemblingFunctions;
}

/**
 * This function will produce an "object assemling function" for each schema in
 * the given array and will also produce a specific "result merging function"
 * for each schema in the given array.
 * @param {Array} schemas An array of schemas in the form of parsed
 * `list-struts`. May also be empty.
 * @returns {Array} The value returned here is actually a tuple consisting of
 * two objects. The first is exactly the same as the output from this functions
 * counterpart (`makeObjectAssemblingFns`). The second is composed of keys made
 * from the names of each schema and "result merging functions" as their
 * corresponding values. Although the tuple is never empty, each object may be
 * empty, if the given input is empty.
 */
export function makeObjectAssemblingAndMergingFns(schemas) {
    const context = makeContext();
    walkSchemas(schemas, events => {
        registerForAssemblingFnConstruction(events, context);
        registerForMergingFnConstruction(events, context);
    });
    const { assemblingFunctions, mergingFunctions } = context;
    return [assemblingFunctions, mergingFunctions];
}
