import md5 from 'md5';
import sha1 from 'sha1';
import { InvalidDigestNameError } from '../errors.mjs';

function wrapDigest(digest) {
    return function wrappedDigest(input = '') {
        return typeof(input) === 'string'
            ? digest(input)
            : digest(JSON.stringify(input));
    };
}

export function selectDigestImplementation(name) {
    if (name === 'md5')
        return wrapDigest(md5);
    if (name === 'sha1')
        return wrapDigest(sha1);
    throw new InvalidDigestNameError(name);
}
