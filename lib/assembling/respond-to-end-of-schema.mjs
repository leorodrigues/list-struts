import { ObjectAssemblageError } from '../errors.mjs';
import { confirmSafeReferenceGraph } from './safeguard.mjs';

function isEmptyMap(object) {
    // eslint-disable-next-line
    return !Object.keys(object).some(k => object.hasOwnProperty(k));
}

function packResult(object, detached) {
    if (isEmptyMap(detached))
        return {object};
    return {object, detached};
}

function ensureContextIsInitialized(context) {
    return context || {ancestors: [], instances: {}};
}

function makeObjectAssemblyFunction(callSequence) {
    return function assembleObject(sequence = [], context) {
        const object = {};
        context = ensureContextIsInitialized(context);
        for (const [attributeName, assembleAttribute] of callSequence) {
            try {
                const value = assembleAttribute(sequence, object, context);
                if (value)
                    object[attributeName] = value;
            } catch(error) {
                throw new ObjectAssemblageError(attributeName, error);
            }
        }
        return packResult(object, context.instances);
    };
}

export function respondToEndOfSchema(context) {
    const {stack, assemblingFunctions} = context;
    const {wholeContentGraph, detachmentGraph} = context;
    return function finishSchema({schemaName}) {
        confirmSafeReferenceGraph(wholeContentGraph, 'whole content');
        confirmSafeReferenceGraph(detachmentGraph, 'detachment');

        const {stageOneCalls, stageTwoCalls, stageThreeCalls} = stack[0];
        const calls = [...stageOneCalls, ...stageTwoCalls, ...stageThreeCalls];
        const assemblyFunction = makeObjectAssemblyFunction(calls);
        assemblingFunctions[schemaName] = assemblyFunction;

        stack.shift();
    };
}
