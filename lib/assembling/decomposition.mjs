import { getField } from '../support.mjs';
import { IncompleteDefinitionError } from '../errors.mjs';

const MSG_ERR_DIGEST_MIN_ARGS =
    'Digest spec. requires an "algorithm name" and at least one "field spec".';

const MSG_ERR_PATTERN_SIZE =
    'Optional parameter "pattern" must have size 1.';

const MSG_ERR_MIN_KEY_ARGS =
    'Key definition requires at least the field name.';

function confirmDefinitionWasGiven(definition) {
    if (isUndefinedOrNull(definition))
        return false;
    if (isEmpty(definition))
        throw new IncompleteDefinitionError();
    return true;
}

function confirmArraySize(size, array, errorMessage) {
    if (array.length !== size)
        throw new IncompleteDefinitionError(errorMessage);
}

function confirmMinArraySize(size, array, errorMessage) {
    if (array.length < size)
        throw new IncompleteDefinitionError(errorMessage);
}

function isEmpty(definition) {
    return !(definition instanceof Array) || definition.length === 0;
}

function isUndefinedOrNull(definition) {
    return undefined === definition || null === definition;
}

export function decomposeKey(definition) {
    confirmMinArraySize(1, definition, MSG_ERR_MIN_KEY_ARGS);
    return [definition[0], definition[1]];
}

export function decomposeType(definition) {
    if (confirmDefinitionWasGiven(definition)) {
        const typeName = definition[0];
        const isArray = definition.indexOf('array') > -1;
        const parameters = [];
        const pattern = getField('pattern', definition);
        if (pattern) {
            confirmArraySize(1, pattern, MSG_ERR_PATTERN_SIZE);
            parameters.push(...pattern);
        }
        return [typeName, isArray, parameters];
    }
}

export function decomposeDigest(definition) {
    if (confirmDefinitionWasGiven(definition)) {
        confirmMinArraySize(2, definition, MSG_ERR_DIGEST_MIN_ARGS);
        const reminder = [...definition];
        return [reminder.shift(), reminder];
    }
}

export function decomposeOptions(definition) {
    if (isUndefinedOrNull(definition) || isEmpty(definition))
        return [false, false];
    const isOptional = definition.indexOf('optional') > -1;
    const isWholeContent = definition.indexOf('whole-content') > -1;
    return [isOptional, isWholeContent];
}

export function decomposeReference(definition) {
    if (confirmDefinitionWasGiven(definition)) {
        const typeName = definition[0];
        const isArray = definition.indexOf('array') > -1;
        const isDetached = definition.indexOf('detached') > -1;
        return [typeName, isArray, isDetached];
    }
}

export function decomposeInheritance(definition) {
    if (confirmDefinitionWasGiven(definition))
        return definition;
}
