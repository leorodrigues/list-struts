import { UnpackError } from './errors.mjs';

const MSG_ERR_MISSING_OBJ_ATTRIBUTE =
    'Result is missing the "object" attribute.';

export function skipFirst(e, i) {
    return i > 0;
}

export function bySchemaAttribute(schemaName) {
    return e => e.schemaName === schemaName;
}

export function byName(name) {
    return e => e[0] === name;
}

export function getField(name, sequence) {
    const result = sequence.find(byName(name));
    return result && result.filter(skipFirst);
}

export function isUndefinedOrEmpty(value) {
    return undefined === value
        || (value instanceof Array && value.length === 0);
}

export function unpack(result) {
    if (result) {
        // eslint-disable-next-line
        if (!result.hasOwnProperty('object'))
            throw new UnpackError(MSG_ERR_MISSING_OBJ_ATTRIBUTE);
        return result.object;
    }
}
